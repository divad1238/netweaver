import org.junit.AfterClass;
import org.junit.BeforeClass;

import netweaver.client.Client;
import netweaver.client.TcpClient;
import netweaver.server.Netweaver;
import netweaver.server.Session;




public class HappyPath {
	private static final Client client = new TcpClient("localhost", 9010);
	
	public static void main(String[] args) throws Exception{
		setup();
	}
	
	@BeforeClass
	public static void setup() throws Exception{
		Netweaver.bootstrap(Session.class);
	}
	
	@AfterClass
	public static void teardown(){
		client.close();
	}
}
