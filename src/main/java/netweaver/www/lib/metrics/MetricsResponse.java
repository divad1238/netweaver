package netweaver.www.lib.metrics;

import java.util.Collection;

import netweaver.metrics.RequestMapperMetric;
import netweaver.metrics.TimerMetric;

public class MetricsResponse{
	public final Collection<RequestMapperMetric> getMappedRequests(){
		return RequestMapperMetric.getMappedRequests();
	}
	
	public final Collection<TimerMetric> getTimedFunctions(){
		return TimerMetric.getTimedFunctions();
	}
}
