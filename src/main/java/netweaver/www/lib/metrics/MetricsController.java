package netweaver.www.lib.metrics;

import netweaver.error.UserException;
import netweaver.server.Session;
import netweaver.server.controller.Controller;

public class MetricsController extends Controller<Object, Session>{

	@Override
	public MetricsResponse handleRequest(Object msg, Session session) throws UserException {
		return new MetricsResponse();
	}
}
