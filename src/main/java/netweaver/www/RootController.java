package netweaver.www;

import netweaver.error.UserException;
import netweaver.server.Session;
import netweaver.server.controller.Controller;

public class RootController extends Controller<Object, Session>{

	@Override
	public Object handleRequest(Object body, Session session) throws UserException {
		return new Object();
	}

	@Override
	public String getUri(){
		return "";
	}
}
