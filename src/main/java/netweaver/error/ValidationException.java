package netweaver.error;

public class ValidationException extends UserException{
	public static final long serialVersionUID = 3519684324562384822L;
	
	public ValidationException(String message){
		super(message);
	}
}
