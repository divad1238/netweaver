package netweaver.error;

public class InternalServerException extends UserException{
	private static final long serialVersionUID = -984321888248448765L;
	
	public InternalServerException(Throwable e){
		super("Internal server error.", e);
	}
}
