package netweaver.error;

public class NetworkException extends UserException{
	public static final long serialVersionUID = -6739572215650517451L;
	
	public NetworkException(){
		super("A netweork related error occured. Please try again later.");
	}
}
