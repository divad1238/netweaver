package netweaver.error;

public class JsonException extends UserException{
	private static final long serialVersionUID = 6360445115400134303L;
	
	public JsonException(String reason){
		super(reason);
	}
}
