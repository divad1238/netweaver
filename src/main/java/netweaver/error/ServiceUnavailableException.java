package netweaver.error;

public class ServiceUnavailableException extends UserException{
	public static final long serialVersionUID = -1584275983145664820L;
	
	public ServiceUnavailableException(){
		super("Service unavailable.");
	}
}
