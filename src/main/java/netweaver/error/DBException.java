package netweaver.error;

import java.sql.SQLException;

public class DBException extends UserException{
	private static final long serialVersionUID = -7366492265801130188L;
	
	public DBException(SQLException cause){
		super(cause);
	}
}
