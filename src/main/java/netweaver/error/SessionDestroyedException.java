package netweaver.error;

public class SessionDestroyedException extends UserException{
	public static final long serialVersionUID = 4728472749961937384L;
	
	public SessionDestroyedException(){
		super("The session was destroyed server side.");
	}
}
