package netweaver.error;

public class InvalidUriException extends UserException{
	public static final long serialVersionUID = -2162309178290773803L;
	
	public InvalidUriException(String uri){
		super(uri + " is not a valid request.");
	}
}
