package netweaver.error;

public class BadRequestException extends UserException{
	public static final long serialVersionUID = -6161557286800661673L;
	
	public BadRequestException(){
		super("Request was malformed.");
	}
}
