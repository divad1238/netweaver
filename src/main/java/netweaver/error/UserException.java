package netweaver.error;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public abstract class UserException extends Exception{
	private static final long serialVersionUID = 6285468153629780629L;
	
	public UserException(String m){
		super(m);
	}
	
	public UserException(Throwable cause){
		super(cause);
	}
	
	public UserException(String m, Throwable cause){
		super(m, cause);
	}
	
	public final String getKey(){
		return this.getClass().getSimpleName();
	}
	
	public final String getStacktrace(){
		Writer result = new StringWriter();
		PrintWriter printWriter = new PrintWriter(result);
		this.printStackTrace(printWriter);
		String stacktrace = result.toString();
		printWriter.close();
		try {result.close();} catch (IOException e2) {}
		return stacktrace;
	}

}
