package netweaver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.base.Function;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import netweaver.server.Netweaver;
import netweaver.util.Util;
import netweaver.util.Util.NumType;

@Slf4j
public final class Localization implements Function<String, String>{
	public static final String LOCALIZATION_PATH = "localizations/";
	private static final ConcurrentHashMap<String, Localization> localizers = new ConcurrentHashMap<>();
	
	private final ConcurrentHashMap<String, Pluralizer> pluralizers = new ConcurrentHashMap<>();
	
	private Localization(String key, Locale l, String path){
		Properties prop = new Properties();
		File f = new File(LOCALIZATION_PATH + (path != null ? path + "/" : "") + key + "/" + l.getLanguage());
		try {
			if(!f.exists() || !f.isFile())throw new IOException();
			prop.load(new BufferedReader(new FileReader(f)));
			if(log.isInfoEnabled())log.info( "Loaded localization file(" + f.getPath() + ").");
		} catch (IOException e) {
			if(log.isWarnEnabled())log.warn( "Missing localization file(" + f.getPath() + ").");
			f = new File(LOCALIZATION_PATH + key + "/" + Netweaver.locale.getLanguage());
			if(!f.exists() || !f.isFile())f = new File(LOCALIZATION_PATH + key + "/" + Netweaver.defaultLocale.getLanguage());
			try {
				if(!f.exists() || !f.isFile())throw new IOException();
				prop.load(new BufferedReader(new FileReader(f)));
				if(log.isInfoEnabled())log.info( "Loaded default localization file(" + f.getPath() + ").");
			} catch (IOException e1) {
				if(log.isWarnEnabled())log.warn( "Failed to load default localization file(" + f.getPath() + ").");
			}
		}
		
		String message;
		int semicolonIndex, lastIndex;
		ArrayList<String> messages = new ArrayList<String>();
		for(Entry<Object, Object> p : prop.entrySet()){
			messages.clear();
			
			message = p.getValue().toString();

			lastIndex = 0;
			while((semicolonIndex = message.indexOf(';', lastIndex)) > 0){
				while(semicolonIndex > 0 && message.charAt(semicolonIndex - 1) == '\\'){
					message = message.substring(0,semicolonIndex - 1) + message.substring(semicolonIndex); 
					semicolonIndex = message.indexOf(';', semicolonIndex);
				}
				if(semicolonIndex > 0){
					messages.add(message.substring(lastIndex, semicolonIndex));
					lastIndex = semicolonIndex + 1;
				}else break;
			}
			if(lastIndex > 0)messages.add(message.substring(lastIndex));
			else messages.add(message);
			
			pluralizers.put(p.getKey().toString(), new Pluralizer(p.getKey().toString(), messages.toArray(new String[messages.size()])));
		}
	}
	
	public static final Localization get(String key, Locale l){
		return get(key, l, null);
	}
	
	public static final Localization get(@NonNull String key, Locale l, String path){
		Localization localizer = localizers.get(key + "." + l == null ? Netweaver.locale.getLanguage() : l.getLanguage());
		if(localizer == null){
			localizer = new Localization(key, l, path);
			localizers.put(key + "." + l.getLanguage(), localizer);
		}return localizer;
	}
	
	@Override
	public final String apply(String key) {
		key = key.trim();
		int spaceIndex = key.indexOf(' ');
		
		String val = null;
		if(spaceIndex >= 0){
			val = key.substring(0, spaceIndex);
			key = key.substring(spaceIndex + 1);
		}
		
		Pluralizer pluralizer = pluralizers.get(key);
		if(pluralizer == null){
			if(log.isWarnEnabled())log.warn("Could not localize key: " + key);
			return "N/A";
		}else return pluralizer.pluralize(val);
	}
	
	private static final class Pluralizer{
		private final String key;
		private final PItem[] pitems;
		private NumType type = null;
		
		public Pluralizer(String key, String[] messages){	
			this.key = key;
			ArrayList<PItem> pitems = new ArrayList<>();
			int hashIndex, commaIndex;
			
			for(String message : messages){
				commaIndex = message.indexOf(',');
				while(commaIndex > 0 && message.charAt(commaIndex - 1) == '\\'){
					message = message.substring(0,commaIndex - 1) + message.substring(commaIndex); 
					commaIndex = message.indexOf(',', commaIndex);
				}
				
				String v1, v2;
				v2 = commaIndex >= 0 ? message.substring(commaIndex + 1) : message;
				
				hashIndex = v2.indexOf('#');
				while(hashIndex > 0 && v2.charAt(hashIndex - 1) == '\\'){
					v2 = v2.substring(0,hashIndex - 1) + v2.substring(hashIndex); 
					hashIndex = v2.indexOf('#', hashIndex);
				}
				
				if(hashIndex >= 0){
					v1 = v2.substring(0, hashIndex);
					v2 = v2.substring(hashIndex + 1);
				}else{
					v1 = null;
				}
				
				if(commaIndex < 0){
					pitems.add(new PItem(0, 0.0, null, v1, v2){

						@Override
						protected boolean matches(int qty) {
							return true;
						}

						@Override
						protected boolean matches(double d) {
							return true;
						}

						@Override
						protected boolean matches(String s) {
							return true;
						}
						
					});
				}else{
					int i = 0;
					double d = 0.0;
					String s = message.substring(
							message.charAt(0) == '=' || 
							message.charAt(0) == '!' || 
							message.charAt(0) == '<' || 
							message.charAt(0) == '>' ? 1 : 0, commaIndex);
					if(type == null)type = Util.isNumeric(s);
					try{
						switch(type){
						case DECIMAL:
							d = Double.parseDouble(s);
							break;
						case INTEGER:
							i = Integer.parseInt(s);
							break;
						default:
						}
					}catch(NumberFormatException e){
						if(log.isWarnEnabled())log.warn("Could not parse pluralizer: " + message);
						continue;
					}
					
					switch(message.charAt(0)){
					case '!':
						pitems.add(new PItem(i, d, s, v1, v2){

							@Override
							protected boolean matches(int i) {
								return this.i != i;
							}
							
							@Override
							protected boolean matches(double d) {
								return Double.compare(this.d, d) != 0;
							}
							
							@Override
							protected boolean matches(String s) {
								return !this.s.equals(s);
							}
							
						});
						break;
					case '>':
						pitems.add(new PItem(i, d, s, v1, v2){

							@Override
							protected boolean matches(int i) {
								return this.i < i;
							}
							
							@Override
							protected boolean matches(double d) {
								return this.d < d;
							}
							
							@Override
							protected boolean matches(String s) {
								return this.s.compareTo(s) < 0;
							}
							
						});
						break;
					case '<':
						pitems.add(new PItem(i, d, s, v1, v2){

							@Override
							protected boolean matches(int i) {
								return this.i > i;
							}
							
							@Override
							protected boolean matches(double d) {
								return this.d > d;
							}
							
							@Override
							protected boolean matches(String s) {
								return this.s.compareTo(s) > 0;
							}
							
						});
						break;
					default:
						pitems.add(new PItem(i, d, s, v1, v2){

							@Override
							protected boolean matches(int i) {
								return this.i == i;
							}
							
							@Override
							protected boolean matches(double d) {
								return this.d == d;
							}
							
							@Override
							protected boolean matches(String s) {
								return this.s.equals(s);
							}
							
						});
					}
				}
			}
			this.pitems = pitems.toArray(new PItem[pitems.size()]);
			if(type == null)type = NumType.NONE;
		}
		
		public final String pluralize(String s){
			try{
				switch(type){
				case DECIMAL:
					double d = Double.parseDouble(s);
					for(PItem pitem : pitems)
						if(pitem.matches(d))return (pitem.value1 == null ? "" : pitem.value1 + d) + pitem.value2;
					break;
				case INTEGER:
					int i = Integer.parseInt(s);
					for(PItem pitem : pitems)
						if(pitem.matches(i))return (pitem.value1 == null ? "" : pitem.value1 + i) + pitem.value2;
					break;
				default:
					for(PItem pitem : pitems)
						if(pitem.matches(s))return (pitem.value1 == null ? "" : pitem.value1 + s) + pitem.value2;
				}
			}catch(NumberFormatException e){
				if(log.isWarnEnabled())log.warn("Could not parse pluralized message: " + s + "is not a(n) " + type.toString().toLowerCase() + ". Key: " + key);
			}
			return null;
		}
		
		@RequiredArgsConstructor
		private static abstract class PItem{
			protected final int i;
			protected final double d;
			protected final String s;
			public final String value1, value2;
			
			protected abstract boolean matches(int i);
			protected abstract boolean matches(double d);
			protected abstract boolean matches(String s);
		}
	}
}
