package netweaver.util;

public class Stack<T>{
	public static final class Node<T>{
		public final T value;
		protected Node<T> next = null;
		
		protected Node(T value){
			this.value = value;
		}
		
		protected void attach(Node<T> item){
			if(next == null)next = item;
			else next.attach(item);
		}
	}
	
	protected Node<T> first = null;
	
	public void push(T value){
		Node<T> item = new Node<T>(value);
		if(first != null)item.attach(first);
		first = item;
	}
	
	public T pop(){
		if(first == null)return null;
		Node<T> item = first;
		first = item.next;
		return item != null ? item.value : null;
	}
	
	public int size(){
		if(first == null)return 0;
		Node<T> node = first;
		int size = 1;
		while((node = node.next) != null)
			size++;
		return size;
	}
	
	public boolean remove(T o){
		if(first == null || o == null)return false;
		
		if(o.equals(first)){
			first = first.next;
			return true;
		}
		
		Node<T> node = first;
		while(node != null){
			if(o.equals(node.next)){
				node.next = node.next.next;
				return true;
			}
			node = node.next;
		}
		
		return false;
	}
	
	public T peek(){
		return first != null ? first.value : null;
	}
}