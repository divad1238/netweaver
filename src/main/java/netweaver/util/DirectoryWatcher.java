package netweaver.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Modifier;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class DirectoryWatcher implements Runnable{
	private final HashSet<File> files = new HashSet<File>();
	private final ConcurrentHashMap<WatchKey, Path> paths = new ConcurrentHashMap<>();
	
	private final WatchService directoryWatcher;
	private final File file;

	public DirectoryWatcher(File file) throws IOException, UnsupportedOperationException{
		directoryWatcher = FileSystems.getDefault().newWatchService();
		this.file = file;
	}
	
	private final void mapFile(File file){
		if(file.isDirectory()){
			File[] files = file.listFiles();
			for(File f : files)mapFile(f);
			Path path = file.toPath().toAbsolutePath();
			try {
				paths.put(path.register(directoryWatcher, new WatchEvent.Kind<?>[]{StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE}, get_com_sun_nio_file_SensitivityWatchEventModifier_HIGH()), path);
			} catch (IOException e) {
				if(log.isErrorEnabled())log.error("Could not watch directory " + file.getAbsolutePath(), e);
			}
    	}else files.add(file);
		try{
			onCreate(file);
		}catch(Throwable t){}
	}
	
	private Modifier get_com_sun_nio_file_SensitivityWatchEventModifier_HIGH() {
	    try {
	      Class<?> c = Class.forName("com.sun.nio.file.SensitivityWatchEventModifier");
	      Field    f = c.getField("HIGH");
	      return (Modifier) f.get(c);
	    } catch (Exception e) {
	      return null;
	    }
	  }

	@Override
	public void run() {
		mapFile(file);
		WatchKey key;
		while(true){
			try {
				key = directoryWatcher.take();
			} catch (InterruptedException | ClosedWatchServiceException e) {
				return;
			}

			try{
				for (WatchEvent<?> event: key.pollEvents()) {
					@SuppressWarnings("unchecked")
					File file = paths.get(key).resolve(((WatchEvent<Path>)event).context()).toFile();

					WatchEvent.Kind<?> kind = event.kind();

					if(kind == StandardWatchEventKinds.ENTRY_CREATE){
						mapFile(file);
					}else if(kind == StandardWatchEventKinds.ENTRY_MODIFY && file.isFile()){
						try{
							onModify(file);
						}catch(Throwable t){}
					}else if(kind == StandardWatchEventKinds.ENTRY_DELETE){
						String path = file.getAbsolutePath();
						Iterator<File> i = files.iterator();
						while(i.hasNext()){
							File f = i.next();
							if(f.getAbsolutePath().startsWith(path + File.separator) || f.getAbsolutePath().equals(path)){
								i.remove();
								try{
									onDelete(f);
								}catch(Throwable t){}
							}
						}
					}
				}
			}finally{
				if(!key.reset()){
					key.cancel();
					paths.remove(key);
				}
			}
		}
	}
	
	public final void close(){
		try {
			directoryWatcher.close();
		} catch (IOException e) {}
	}

	protected abstract void onCreate(File file);
	protected abstract void onModify(File file);
	protected abstract void onDelete(File file);
}
