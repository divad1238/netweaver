package netweaver.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLException;

import lombok.extern.slf4j.Slf4j;
import netweaver.interfaces.Service;
import netweaver.server.Netweaver;
import netweaver.server.Server;

@Slf4j
public final class Util {
	private Util(){}
	
	public static enum NumType{
		INTEGER, DECIMAL, NONE
	}
	
	public static final NumType isNumeric(String string){
		if(string == null || string.isEmpty())return NumType.NONE;
		int i = 0;
		if (string.charAt(0) == '-') {
			if(string.length() > 1) i++;
			else return NumType.NONE;
		}
		int n = string.length();
		char c;
		boolean decimal = false;
		for (; i < n; i++) {
			c = string.charAt(i);
			switch(c){
			case '.':
				if(decimal)return NumType.NONE;
				else{
					if(i == string.length() - 1)return NumType.NONE;
					else decimal = true;
				}
				break;
			default:
				if(!Character.isDigit(c))return NumType.NONE;
			}
		}return decimal ? NumType.DECIMAL : NumType.INTEGER;
	}
	
	/*private static final char[] PASSWORD = "6HHf*Xr_Eh<}RWd32>qn3#,7S('f%a.?".toCharArray();
	private static final byte[] SALT = {
        (byte) 0xad, (byte) 0x84, (byte) 0x9c, (byte) 0xa5,
        (byte) 0x12, (byte) 0x77, (byte) 0x55, (byte) 0x33,
    };*/
	
	public static final String crypt(String toCrypt) throws SSLException{
		try {
			return new String(MessageDigest.getInstance("MD5").digest(toCrypt.getBytes(Netweaver.charset)));
		} catch (NoSuchAlgorithmException e) {
			throw new SSLException(e);
		}
	}
	
	/*public static final String encrypt(String toEncrypt){
        try {
        	SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
            SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
            Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
			pbeCipher.init(Cipher.ENCRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
			return Base64.encodeToString(pbeCipher.doFinal(toEncrypt.getBytes(Netweaver.charset)), false);
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
	
	public static final String decrypt(String toDecrypt) throws SSLException{
        try {
        	SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
            SecretKey key = keyFactory.generateSecret(new PBEKeySpec(PASSWORD));
            Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
            pbeCipher.init(Cipher.DECRYPT_MODE, key, new PBEParameterSpec(SALT, 20));
			return new String(pbeCipher.doFinal(Base64.decode(toDecrypt)), Netweaver.charset);
		}  catch (Exception e) {
			throw new RuntimeException(e);
		}
	}*/
	
	public static final boolean shutdown(final boolean restart, final Service...services){
		final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
		final ArrayList<String> command = new ArrayList<String>();
		
		if(restart){
			File currentJar;
			try {
				currentJar = new File(Server.class.getProtectionDomain().getCodeSource().getLocation().toURI());
				if(!currentJar.getName().endsWith(".jar"))throw new URISyntaxException(currentJar.getName(), "cannot restart. not in jar.");	
			} catch (URISyntaxException e) {
				return false;
			}
			
			command.add(javaBin);
			command.add("-jar");
			command.add(currentJar.getPath());
		}
		
		for(Service service : services)
			service.stop();
		
		new Thread(new Runnable(){

			@Override
			public void run() {
				for(Service service : services)
					service.stop();
				if(restart){
					final ProcessBuilder builder = new ProcessBuilder(command);
					try {
						builder.start();
					} catch (IOException e) {
						for(Service service : services)
							service.start();
						if(log.isErrorEnabled())log.error("Could not restart server.", e);
					}
				}
				System.exit(0);
			}
			
		}).start();
		
		return true;
	}
	
	public static final Class<?> getGenericClass(Object instance, Class<?> classOfInterest, int parameterIndex) {
		Map<Type, Type> typeMap = new HashMap<Type, Type>();
		Class<?> instanceClass = instance.getClass();
		if(classOfInterest != instanceClass){
			while (classOfInterest != instanceClass.getSuperclass()) {
				extractTypeArguments(typeMap, instanceClass);
				instanceClass = instanceClass.getSuperclass();
				if (instanceClass == null) throw new IllegalArgumentException();
			}
		}

		ParameterizedType parameterizedType = (ParameterizedType) instanceClass.getGenericSuperclass();
		Type actualType = parameterizedType.getActualTypeArguments()[parameterIndex];
		if (typeMap.containsKey(actualType)) {
			actualType = typeMap.get(actualType);
		}

		if (actualType instanceof Class) {
			return (Class<?>) actualType;
		} else if (actualType instanceof TypeVariable) {
			return browseNestedTypes(instance, (TypeVariable<?>) actualType);
		} else {
			throw new IllegalArgumentException();
		}
	}

	private static final void extractTypeArguments(Map<Type, Type> typeMap, Class<?> clazz) {
		Type genericSuperclass = clazz.getGenericSuperclass();
		if (!(genericSuperclass instanceof ParameterizedType)) {
			return;
		}

		ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
		Type[] typeParameter = ((Class<?>) parameterizedType.getRawType()).getTypeParameters();
		Type[] actualTypeArgument = parameterizedType.getActualTypeArguments();
		for (int i = 0; i < typeParameter.length; i++) {
			if(typeMap.containsKey(actualTypeArgument[i])) {
				actualTypeArgument[i] = typeMap.get(actualTypeArgument[i]);
			}
			typeMap.put(typeParameter[i], actualTypeArgument[i]);
		}
	}

	private static final Class<?> browseNestedTypes(Object instance, TypeVariable<?> actualType) {
		Class<?> instanceClass = instance.getClass();
		List<Class<?>> nestedOuterTypes = new LinkedList<Class<?>>();
		for (
				Class<?> enclosingClass = instanceClass.getEnclosingClass();
				enclosingClass != null;
				enclosingClass = enclosingClass.getEnclosingClass()) {
			try {
				Field this$0 = instanceClass.getDeclaredField("this$0");
				Object outerInstance = this$0.get(instance);
				Class<?> outerClass = outerInstance.getClass();
				nestedOuterTypes.add(outerClass);
				Map<Type, Type> outerTypeMap = new HashMap<Type, Type>();
				extractTypeArguments(outerTypeMap, outerClass);
				for (Map.Entry<Type, Type> entry : outerTypeMap.entrySet()) {
					if (!(entry.getKey() instanceof TypeVariable)) {
						continue;
					}
					TypeVariable<?> foundType = (TypeVariable<?>) entry.getKey();
					if (foundType.getName().equals(actualType.getName())
							&& isInnerClass(foundType.getGenericDeclaration(), actualType.getGenericDeclaration())) {
						if (entry.getValue() instanceof Class) {
							return (Class<?>) entry.getValue();
						}
						actualType = (TypeVariable<?>) entry.getValue();
					}
				}
			} catch (NoSuchFieldException e) { /* this should never happen */ } catch (IllegalAccessException e) { /* this might happen */}

		}
		throw new IllegalArgumentException();
	}

	private static final boolean isInnerClass(GenericDeclaration outerDeclaration, GenericDeclaration innerDeclaration) {
		if (!(outerDeclaration instanceof Class) || !(innerDeclaration instanceof Class)) {
			throw new IllegalArgumentException();
		}
		Class<?> outerClass = (Class<?>) outerDeclaration;
		Class<?> innerClass = (Class<?>) innerDeclaration;
		while ((innerClass = innerClass.getEnclosingClass()) != null) {
			if (innerClass == outerClass) {
				return true;
			}
		}
		return false;
	}
}
