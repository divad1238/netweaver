package netweaver.util;

public final class Queue<T> extends Stack<T>{
	private Node<T> last = null;
	
	@Override
	public void push(T value){
		Node<T> item = new Node<T>(value);
		if(last != null)last.attach(item);
		else first = item;
		last = item;
	}
	
	@Override
	public T pop(){
		if(first == null)return null;
		Node<T> item = first;
		first = item.next;
		if(first == null)last = null;
		return item != null ? item.value : null;
	}
	
	public boolean remove(T o){
		throw new Error("Method not implemented");
	}
}