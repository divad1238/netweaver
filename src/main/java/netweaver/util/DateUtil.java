package netweaver.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import netweaver.server.Netweaver;

public final class DateUtil {
	private DateUtil(){}
	
	private static final SimpleDateFormat datePrinter = new SimpleDateFormat("yyyy-MM-dd", Netweaver.defaultLocale);
	private static final SimpleDateFormat timePrinter = new SimpleDateFormat("HH:mm:ss", Netweaver.defaultLocale);

	public static final Date getDate(){
		return Calendar.getInstance(Netweaver.locale).getTime();
	}

	public static final String getSQLTime(){
		return timePrinter.format(Calendar.getInstance(Netweaver.locale).getTime());
	}

	public static final String getSQLTime(Date date){
		return timePrinter.format(date);
	}

	public static final String getSQLDate(){
		return datePrinter.format(Calendar.getInstance(Netweaver.locale).getTime());
	}

	public static final String getSQLDate(Date date){
		return datePrinter.format(date);
	}

	public static final String getSQLDateTime(){
		return getSQLDate() + " " + getSQLTime();
	}

	public static final String getSQLDateTime(Date date){
		return getSQLDate(date) + " " + getSQLTime(date);
	}

	public static final int getDayOfWeek(){
		return Calendar.getInstance(Netweaver.locale).get(Calendar.DAY_OF_WEEK);
	}

	public static final int getDayOfMonth(){
		return Calendar.getInstance(Netweaver.locale).get(Calendar.DAY_OF_MONTH);
	}

	public static final int getMonth(){
		return Calendar.getInstance(Netweaver.locale).get(Calendar.MONTH);
	}

	public static final int getYear(){
		return Calendar.getInstance(Netweaver.locale).get(Calendar.YEAR);
	}
}
