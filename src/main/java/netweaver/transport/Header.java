package netweaver.transport;

import java.util.Locale;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import netweaver.Localization;
import netweaver.server.Netweaver;

public class Header{
	public static enum Type{
		TCP, WEBSOCKET, GET, POST, PUT, HEAD, OPTIONS, PATCH, DELETE, TRACE, CONNECT
	}
	
	@Getter @Setter @NotNull
	private String uri, uid;
	@Getter @Setter
	private boolean isError;
	
	@JsonIgnore @Getter @Setter
	private Type type;
	@JsonIgnore
	private Locale lang;
	
	@JsonIgnore @Getter
	private String body;
	@JsonIgnore @Getter @Setter
	private boolean keepAlive, binary;
	@JsonIgnore @Getter @Setter
	private String ifModifiedSince; 
	
	@JsonIgnore @Getter @Setter
	private Object tag;
	
	@JsonIgnore
	private Localization localize;
	
	@SuppressWarnings("unused")
	private Header(){}
	
	public Header(String uri, Object body){
		this.uri = uri;
		this.body = Netweaver.toJson(body);
		this.isError = body instanceof ErrorMessage;
	}
	
	public Header(String uri, String body, boolean isError){
		this.uri = uri;
		this.body = body;
		this.isError = isError;
	}
	
	@JsonIgnore
	public void setBody(String body, boolean isError){
		this.body = body;
		this.isError = isError;
	}
	
	@JsonIgnore
	public void setBody(Object body){
		this.body = Netweaver.toJson(body);
		this.isError = body instanceof ErrorMessage;
	}
	
	@JsonIgnore
	public void setLang(@NonNull Locale lang){
		this.lang = lang;
	}
	
	@JsonIgnore
	public void setLang(String lang){
		if(lang == null)return;
		setLang(Locale.forLanguageTag(lang));
	}
	
	@JsonIgnore
	public Locale getLang(){
		return lang == null ? Netweaver.locale : lang;
	}
	
	@JsonAnySetter
	public void jsonAnySetter(String key, String value){
		if(key == null || value == null || value.isEmpty())return;
		switch(key){
		case "type":
			setType(Type.valueOf(value));
			break;
		case "lang":
			setLang(value);
			break;
		}
	}
	
	@JsonIgnore
	public Localization getLocalize(){
		if(localize == null)localize = Localization.get(getUri(), getLang(), "messages");
		return localize;
	}
}
