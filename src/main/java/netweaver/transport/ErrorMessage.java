package netweaver.transport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import netweaver.error.UserException;

public final class ErrorMessage{
	private final String key,  message;
	
	@JsonIgnore
	private final UserException e;
	
	public ErrorMessage(UserException error){
		this.e = error;
		this.key = null;
		this.message = null;
	}
	
	@JsonCreator
	public ErrorMessage(@JsonProperty("key") String key, @JsonProperty("message") String message){
		this.key = key;
		this.message = message;
		this.e = null;
	}
	
	public String getKey(){
		return e == null ? key : e.getKey();
	}
	
	public String getMessage(){
		return e == null ? message : e.getMessage();
	}
	
	public boolean isOfType(Class<? extends UserException> e){
		if(e == null || getKey() == null)return false;
		return getKey().equals(e.getSimpleName());
	}
	
	public boolean isOfType(UserException e){
		if(e == null || getKey() == null)return false;
		return getKey().equals(e.getKey());
	}
}
