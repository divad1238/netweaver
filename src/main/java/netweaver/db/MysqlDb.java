package netweaver.db;
import java.sql.Connection;
import java.sql.SQLException;

import netweaver.interfaces.Db;
import netweaver.server.Netweaver;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;


public abstract class MysqlDb implements Db{
	private HikariDataSource dataSource;
	
	public Connection getConnection() throws SQLException{
		if(dataSource == null)throw new IllegalAccessError("open datasource first.");
		return dataSource.getConnection();
	}
	
	@Override
	public void start(){
		if(dataSource == null){
			final HikariConfig hConf = new HikariConfig();
			hConf.setMaximumPoolSize(Math.max(Netweaver.coreCount * 2 + Netweaver.spindleCount, 10));
			hConf.setAutoCommit(false);
			hConf.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
			hConf.setConnectionTimeout(30000);
			
			hConf.addDataSourceProperty("serverName", getHost());
			hConf.addDataSourceProperty("port", getPort());
			hConf.addDataSourceProperty("databaseName", getDbName());
			hConf.addDataSourceProperty("user", getUser());
			hConf.addDataSourceProperty("password", getPassword());

			hConf.setLeakDetectionThreshold(5000);
			hConf.addDataSourceProperty("cachePrepStmts", true);
			hConf.addDataSourceProperty("prepStmtCacheSize", 250);
			hConf.addDataSourceProperty("prepStmtCacheSqlLimit", 2048);
			hConf.addDataSourceProperty("useServerPrepStmts", true);
			hConf.addDataSourceProperty("useLocalSessionState", true);
			hConf.addDataSourceProperty("useLocalTransactionState", true);
			hConf.addDataSourceProperty("rewriteBatchedStatements", true);
			hConf.addDataSourceProperty("cacheResultSetMetadata", true);
			hConf.addDataSourceProperty("cacheServerConfiguration", true);
			hConf.addDataSourceProperty("elideSetAutoCommits", true);
			hConf.addDataSourceProperty("maintainTimeStats", false);
			dataSource = new HikariDataSource(hConf);
		}else throw new IllegalAccessError("datasource already opened.");
	}
	
	@Override
	public void stop(){
		if(dataSource != null){
			dataSource.close();
			dataSource = null;
		}
	}

	public abstract String getHost();
	public abstract int getPort();
	public abstract String getDbName();
	public abstract String getUser();
	public abstract String getPassword();
}
