package netweaver.interfaces;



public interface Service {
	public void start();
	public void stop();
}
