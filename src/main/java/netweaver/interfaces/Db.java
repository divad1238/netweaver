package netweaver.interfaces;

import java.sql.Connection;
import java.sql.SQLException;

public interface Db extends Service{
	public Connection getConnection() throws SQLException;
}
