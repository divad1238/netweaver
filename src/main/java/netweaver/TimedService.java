package netweaver;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import netweaver.interfaces.Service;

public abstract class TimedService implements Service, Runnable{
	private static ScheduledExecutorService executor;
	
	private ScheduledFuture<TimedService> scheduledFuture;

	@Override
	public final void start() {
		if(executor == null)executor = Executors.newScheduledThreadPool(1);
		if(getInterval() <= 0)return;
		executor.scheduleAtFixedRate(this, runOnStart() ? 0 : getInterval(), getInterval(), getTimeUnit());
		doStart();
	}

	@Override
	public final void stop() {
		if(scheduledFuture != null)scheduledFuture.cancel(false);
		doStop();
	}
	
	public TimeUnit getTimeUnit(){
		return TimeUnit.SECONDS;
	}
	
	public boolean runOnStart(){
		return true;
	}
	
	protected abstract void doStart();
	protected abstract void doStop();
	public abstract long getInterval();
}
