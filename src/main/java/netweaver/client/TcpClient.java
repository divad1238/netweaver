package netweaver.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.ssl.SslContext;
import netweaver.client.netty.ClientMessageHandler;
import netweaver.server.Session;
import netweaver.server.netty.TcpCodec;

public class TcpClient extends Client{

	private final SslContext sslCtx;
	private EventLoopGroup group = null;
	
	public TcpClient(String host, int port) {
		this(host, port, null, null);
	}
	
	public TcpClient(String host, int port, SslContext sslCtx) {
		this(host, port, sslCtx, null);
	}
	
	public TcpClient(String host, int port, SslContext sslCtx, Class<? extends Session> sessionClass) {
		super(host, port, sessionClass);
		this.sslCtx = sslCtx;
	}

	@Override
	public ChannelFuture connect() {
		if(isConnected())close();
		Bootstrap b = new Bootstrap();
		b.group(group = new NioEventLoopGroup(1))
		.channel(NioSocketChannel.class)
		.option(ChannelOption.TCP_NODELAY, true)
		.handler(new ChannelInitializer<SocketChannel>() {

			@Override
			public void initChannel(SocketChannel ch) throws Exception {
				ChannelPipeline p = ch.pipeline();
				if (sslCtx != null)p.addLast(sslCtx.newHandler(ch.alloc(), getHost(), getPort()));
				//p.addLast(ZlibCodecFactory.newZlibEncoder(ZlibWrapper.GZIP));//TODO GZIP from constructor
			    //p.addLast(ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP));
				p.addLast(new DelimiterBasedFrameDecoder(1024 * 512, Delimiters.lineDelimiter()));//TODO buffer size from constructor
				p.addLast(TcpCodec.get());
				p.addLast(new ClientMessageHandler(TcpClient.this));
			}
		});
		
		return b.connect(getHost(), getPort());
	}
	
	@Override
	public boolean close() {
		if(group != null){
			group.shutdownGracefully();
			group = null;
		}
		return super.close();
	}
}
