package netweaver.client;

import java.net.URI;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshakerFactory;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketHandshakeException;
import io.netty.handler.codec.http.websocketx.WebSocketVersion;
import io.netty.handler.ssl.SslContext;
import io.netty.util.CharsetUtil;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class WebSocketClient{
	private final SslContext sslCtx;
	private EventLoopGroup group = null;
	private WebSocketClientHandshaker handshaker;

	@Getter
	private String host;
	@Getter
	private int port;

	public WebSocketClient(String host, int port) {
		this(host, port, null);
	}

	public WebSocketClient(String host, int port, SslContext sslCtx) {
		this.host = host;
		this.port = port;
		this.sslCtx = sslCtx;
	}

	public ChannelFuture connect() {
		if(isOpen())group.shutdownGracefully();
		// Connect with V13 (RFC 6455 aka HyBi-17). You can change it to V08 or V00.
		// If you change it to V00, ping is not supported and remember to change
		// HttpResponseDecoder to WebSocketHttpResponseDecoder in the pipeline.
		this.handshaker = WebSocketClientHandshakerFactory.newHandshaker(
				URI.create(sslCtx == null ? "ws://" : "wss://" + getHost() + ":" + getPort()), 
				WebSocketVersion.V13, null, false, new DefaultHttpHeaders());//mustn't reuse

		Bootstrap b = new Bootstrap();
		b.group(group = new NioEventLoopGroup(1))
		.channel(NioSocketChannel.class)
		.option(ChannelOption.TCP_NODELAY, true)
		.handler(new ChannelInitializer<SocketChannel>() {

			@Override
			public void initChannel(SocketChannel ch) throws Exception {
				ChannelPipeline p = ch.pipeline();
				if (sslCtx != null)p.addLast(sslCtx.newHandler(ch.alloc(), getHost(), getPort()));
				p.addLast(new HttpClientCodec());
				p.addLast(new HttpObjectAggregator(8192));
				p.addLast(new SimpleChannelInboundHandler<Object>() {//must be @shareable
					@Override
					public void channelActive(ChannelHandlerContext ctx) {
						handshaker.handshake(ctx.channel());
					}

					@Override
					public void channelInactive(ChannelHandlerContext ctx) {
						log.info("WebSocket Client disconnected!");
						onClose();
					}

					@Override
					public void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
						Channel ch = ctx.channel();
						if (!handshaker.isHandshakeComplete()) {
							try {
								handshaker.finishHandshake(ch, (FullHttpResponse) msg);
								//System.out.println("WebSocket Client connected!");
								onOpen(ch);
							} catch (WebSocketHandshakeException e) {
								//System.out.println("WebSocket Client failed to connect");
								throw e;
							}
							return;
						}

						if (msg instanceof FullHttpResponse) {
							FullHttpResponse response = (FullHttpResponse) msg;
							throw new IllegalStateException(
									"Unexpected FullHttpResponse (getStatus=" + response.status() +
									", content=" + response.content().toString(CharsetUtil.UTF_8) + ')');
						}

						WebSocketFrame frame = (WebSocketFrame) msg;
						if (frame instanceof TextWebSocketFrame) {
							//System.out.println("WebSocket Client received message.");
							onReceive(((TextWebSocketFrame) frame).text());
						} else if (frame instanceof PongWebSocketFrame) {
							//System.out.println("WebSocket Client received pong");
						} else if (frame instanceof CloseWebSocketFrame) {
							log.info("WebSocket Client received closing");
							ch.close();
							onClose();
						}
					}

					@Override
					public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
						onError(cause);
					}
				});
			}
		});

		return b.connect(getHost(), getPort());
	}

	public void close() {
		if(isOpen()){
			group.shutdownGracefully();
			group = null;
			onClose();
		}
	}

	public boolean isOpen() {
		return group!= null && !(group.isShutdown() || group.isShuttingDown() || group.isTerminated());
	}

	public abstract void onOpen(Channel ch);
	public abstract void onReceive(String msg);
	public abstract void onClose();
	public abstract void onError(Throwable e);
}
