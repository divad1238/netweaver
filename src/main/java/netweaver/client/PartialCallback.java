package netweaver.client;

import java.util.concurrent.TimeUnit;

import netweaver.transport.Header;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class PartialCallback{
	@Getter
	private final int timeout;
	@Getter
	private final TimeUnit timeUnit;
	
	public PartialCallback(){
		this(2000, TimeUnit.MILLISECONDS);
	}
	
	public abstract void handleResponse(Header header) throws Throwable;
}
