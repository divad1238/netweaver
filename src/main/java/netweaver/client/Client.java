package netweaver.client;

import java.lang.reflect.InvocationTargetException;
import java.util.Locale;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import netweaver.error.ServiceUnavailableException;
import netweaver.server.Session;

@Slf4j
public abstract class Client{
	@Getter
	private String host;
	@Getter
	private int port;

	private final Class<? extends Session> sessionClass;

	private Session session = null;
	
	public Client(String host, int port){
		this(host, port, null);
	}

	public Client(String host, int port, Class<? extends Session> sessionClass){
		this.host = host;
		this.port = port;
		this.sessionClass = sessionClass != null && sessionClass != Session.class ? sessionClass : Session.class;
		init(null);
	}

	public abstract ChannelFuture connect();

	public void init(String uid) {
		if(session != null){
			if(session.getUid() == null || session.getUid().isEmpty()){
				session.init(uid);
			}else{
				try{
					session.destroy();
				}catch(Throwable t){
					if(log.isErrorEnabled())log.error("Error destroying session.", t);
				}
				session = null;
			}
		}else{
			try {
				try {
					session = sessionClass.getDeclaredConstructor().newInstance();
				} catch (IllegalArgumentException | InvocationTargetException | NoSuchMethodException
						| SecurityException e) {
					throw new RuntimeException(e);
				}
			} catch (InstantiationException | IllegalAccessException e) {
				if(log.isErrorEnabled())log.error("Could not instantiate session via reflection (likely no default constructor). Using DefaultSession instead.", e);
				session = new Session();
			}
			session.init(uid);
		}
	}

	public String getUid() {
		return session.getUid();
	}
	
	protected void setConnectionDetails(String host, int port){
		this.host = host;
		this.port = port;
		if(isConnected())close();
	}

	/**
	 * Caution: current version of Netty does not call ChannelFutureListener.operationComplete(..). TODO Test using newer versions.
	 */
	public ChannelFuture write(String uri, String message, boolean isError) throws ServiceUnavailableException {
		return session.write(uri, message, isError);
	}
	
	public ChannelFuture write(String uri, Object message) throws ServiceUnavailableException {
		return session.write(uri, message);
	}

	public void request(final String uri, final String message, final boolean isError, final PartialCallback callback) {
		if(!session.isOpen())connect().addListener(new ChannelFutureListener(){

			@Override
			public void operationComplete(ChannelFuture f)throws Exception {
				if(f.isSuccess())session.setChannel(f.channel());
				session.request(uri, message, isError, callback);
			}

		});
		else session.request(uri, message, isError, callback);
	}
	
	public void request(final String uri, final Object message, final PartialCallback callback) {
		if(!session.isOpen())connect().addListener(new ChannelFutureListener(){

			@Override
			public void operationComplete(ChannelFuture f)throws Exception {
				if(f.isSuccess())session.setChannel(f.channel());
				session.request(uri, message, callback);
			}

		});
		else session.request(uri, message, callback);
	}
	
	public boolean invokeHandler(String uri, String message, boolean isError) {
		return session.invokeHandler(uri, message, isError);
	}

	public boolean invokeHandler(String uri, Object message) {
		return session.invokeHandler(uri, message);
	}
	
	public void setDefaultCallback(String uri, PartialCallback callback) {
		session.setDefaultCallback(uri, callback);
	}
	
	public PartialCallback getCallback(String uri) {
		return session.getCallback(uri);
	}
	
	public PartialCallback getDefaultCallback(String uri) {
		return session.getDefaultCallback(uri);
	}

	public void setPreferredLocale(Locale l) {
		session.setPreferredLocale(l);
	}

	public Locale getPreferredLocale() {
		return session.getPreferredLocale();
	}

	public boolean close() {
		return session.closeChannel();
	}

	public boolean isConnected() {
		return session.isOpen();
	}
	
	public Channel getChannel(){
		return session.getChannel();
	}
	
}
