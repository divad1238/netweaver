package netweaver.client;

import java.util.concurrent.TimeUnit;

import netweaver.server.Netweaver;
import netweaver.transport.ErrorMessage;
import netweaver.transport.Header;
import netweaver.util.Util;

public abstract class Callback<ResponseType> extends PartialCallback{
	private Class<ResponseType> responseClass = null;
	
	public Callback(){
		super();
	}
	
	public Callback(int timeout, TimeUnit timeUnit){
		super(timeout, timeUnit);
	}
	
	@SuppressWarnings("unchecked")
	public final Class<ResponseType> getResponseClass(){
		if(responseClass == null){
			responseClass = (Class<ResponseType>) Util.getGenericClass(this, Callback.class, 0);
		}return responseClass;
	}
	
	@Override
	public void handleResponse(Header header) throws Throwable{
		if(header.isError())_handleError(Netweaver.fromJson(header.getBody(), ErrorMessage.class));
		else _handleResponse(Netweaver.fromJson(header.getBody(), getResponseClass()));
	}
	
	protected abstract void _handleResponse(ResponseType msg) throws Throwable;
	protected abstract void _handleError(ErrorMessage msg) throws Throwable;
}
