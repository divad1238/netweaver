package netweaver.client.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import netweaver.client.Client;
import netweaver.error.InvalidUriException;
import netweaver.transport.Header;

@Slf4j
@RequiredArgsConstructor
public class ClientMessageHandler extends SimpleChannelInboundHandler<Header>{
	private final Client client;

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Header msg)throws Exception {
		if(!msg.getUid().equals(client.getUid()))client.init(msg.getUid());
		if(!client.invokeHandler(msg.getUri(), msg.getBody(), msg.isError()))throw new InvalidUriException(msg.getUri());
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)throws Exception {
		if(cause instanceof InvalidUriException)if(log.isWarnEnabled())log.warn("Unhandled response with uri: " + ((InvalidUriException)cause).getMessage());
		else ctx.fireExceptionCaught(cause);
	}
}
