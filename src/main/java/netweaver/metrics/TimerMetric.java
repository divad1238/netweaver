package netweaver.metrics;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import netweaver.server.Netweaver;

/* Metrics incurs a performance penalty.
 * Reported values include a portion of this penalty.
 * Take results with a grain of salt (i.e. compare apples to apples).
 */ 

@RequiredArgsConstructor(access=AccessLevel.PROTECTED)
public class TimerMetric{
	@JsonIgnore
	private static final ConcurrentHashMap<String, TimerMetric> timedFunctions = new ConcurrentHashMap<String, TimerMetric>();
	
	@JsonIgnore
	public final long startTime;	
	@JsonIgnore
	private final AtomicLong counter = new AtomicLong(0);
	@JsonIgnore
	private final AtomicLong errCounter = new AtomicLong(0);
	@JsonIgnore
	private final AtomicLong totalDuration = new AtomicLong(0);
	
	@JsonIgnore
	private volatile long maxExecutionTime = 0;
	@JsonIgnore
	private volatile long minExecutionTime = Long.MAX_VALUE;
	
	@JsonInclude
	public final String function;
	
	@RequiredArgsConstructor
	public static class Promise{
		protected final long startTime = System.nanoTime();
		protected final String key;
		@Getter @Setter
		boolean error = false;
	}
	
	public static final Promise start(){
		if(!Netweaver.isMetered)return null;
		StackTraceElement elem = Thread.currentThread().getStackTrace()[2];
		return new Promise(elem.getClassName() + "." + elem.getMethodName() + "(" + elem.getLineNumber() + ")");
	}
	
	public static final Promise start(String key){
		if(!Netweaver.isMetered)return null;
		return new Promise(key);
	}
	
	public static final TimerMetric finish(@NonNull Promise promise){
		if(!Netweaver.isMetered)return null;
		return finish(System.nanoTime(), promise, timedFunctions);
	}
	
	protected static final TimerMetric finish(long temp, Promise promise, ConcurrentHashMap<String, TimerMetric> timedFunctions){
		TimerMetric tm = timedFunctions.get(promise.key);
		if(tm == null)timedFunctions.putIfAbsent(promise.key, tm = new TimerMetric(promise.startTime, promise.key));
		
		temp -= promise.startTime;
		
		tm.counter.incrementAndGet();
		if(promise.error)tm.errCounter.incrementAndGet();
		tm.totalDuration.addAndGet(temp);
		if(tm.maxExecutionTime < temp)tm.maxExecutionTime = temp;
		if(tm.minExecutionTime > temp)tm.minExecutionTime = temp;
		
		return tm;
	}
	
	@JsonInclude
	public final double getMonitorRuntimeSecs(){
		return Math.round(((System.nanoTime() - startTime) / 1000000000D) * 1000D) / 1000D;
	}
	
	@JsonInclude
	public final double getTotalExecutionSecs(){
		return Math.round((totalDuration.get() / 1000000000D) * 1000D) / 1000D;
	}
	
	@JsonInclude
	public final long getExecutionCount(){
		return counter.get();
	}
	
	@JsonInclude
	public final long getErrorCount(){
		return errCounter.get();
	}
	
	@JsonInclude
	public final double getSlowestMillis(){
		return Math.round((maxExecutionTime / 1000000D) * 1000D) / 1000D; 
	}
	
	@JsonInclude
	public final double getAverageMillis(){
		return Math.round((totalDuration.get() / (1000000D * counter.get())) * 1000D) / 1000D;
	}
	
	@JsonInclude
	public final double getFastestMillis(){
		return Math.round((minExecutionTime / 1000000D) * 1000D) / 1000D; 
	}
	
	@JsonInclude
	public final double getExecutionsPerSecond(){
		if(counter.get() == 0)return 0;
		return Math.round((1000000000D / ((System.nanoTime() - startTime) / counter.get())) * 100D) / 100D;
	}
	
	@JsonInclude
	public final double getMaxExecutionsPerSecond(){
		if(minExecutionTime == 0)return 0;
		return Math.round((1000000000D / minExecutionTime) * 100D) / 100D;
	}
	
	@JsonInclude
	public final double getAverageMaxExecutionsPerSecond(){
		if(counter.get() == 0)return 0;
        return Math.round((1000000000D / (totalDuration.get() / counter.get())) * 100D) / 100D;
	}
	
	@JsonInclude
	public final double getMinExecutionsPerSecond(){
		if(maxExecutionTime == 0)return 0;
		return Math.round((1000000000D / maxExecutionTime) * 100D) / 100D;
	}
	
	@JsonIgnore
	public static final Collection<TimerMetric> getTimedFunctions(){return timedFunctions.values();}
	
}
