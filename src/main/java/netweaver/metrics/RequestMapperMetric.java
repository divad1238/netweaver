package netweaver.metrics;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import netweaver.server.Netweaver;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public class RequestMapperMetric{
	@JsonIgnore
	private static final ConcurrentHashMap<String, RequestMapperMetric> mappedRequests = new ConcurrentHashMap<String, RequestMapperMetric>();
	
	@NonNull @JsonInclude
	public final String uuid;
	@JsonIgnore
	private final ConcurrentHashMap<String, TimerMetric> timedFunctions = new ConcurrentHashMap<String, TimerMetric>();
	
	public static class Promise extends TimerMetric.Promise{
		public final String uuid;
		protected Promise(String key, String uuid) {
			super(key);
			this.uuid = uuid;
		}
		
	}
	
	@JsonInclude
	public final Collection<TimerMetric> getTimedFunctions(){
		return timedFunctions.values();
	}
	
	public static final Promise start(String uuid){
		if(!Netweaver.isMetered)return null;
		StackTraceElement elem = Thread.currentThread().getStackTrace()[2];
		return new Promise(elem.getClassName() + "." + elem.getMethodName() + "(" + elem.getLineNumber() + ")", uuid);
	}
	
	public static final Promise start(String uuid, String key){
		if(!Netweaver.isMetered)return null;
		return new Promise(key, uuid);
	}
	
	public static final RequestMapperMetric finish(@NonNull Promise promise){
		if(!Netweaver.isMetered)return null;
		long temp = System.nanoTime();
		RequestMapperMetric rmm = mappedRequests.get(promise.uuid);
		if(rmm == null)mappedRequests.putIfAbsent(promise.uuid, rmm = new RequestMapperMetric(promise.uuid));
		TimerMetric.finish(temp, promise, rmm.timedFunctions);
		return rmm;
	}
	
	@JsonIgnore
	public static final Collection<RequestMapperMetric> getMappedRequests(){return mappedRequests.values();}
}
