package netweaver.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.tika.Tika;
import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.template.PebbleTemplate;

import javassist.Modifier;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import netweaver.annotations.Inject;
import netweaver.error.JsonException;
import netweaver.error.ValidationException;
import netweaver.interfaces.Service;
import netweaver.server.controller.BaseController;
import netweaver.template.Template;
import netweaver.template.TemplateType;
import netweaver.template.mustache.HtmlMustacheFactory;
import netweaver.template.mustache.PlainTextMustacheFactory;
import netweaver.transport.Header;
import netweaver.transport.jackson.DateSerializer;
import netweaver.util.DirectoryWatcher;

@SuppressWarnings({ "unchecked", "rawtypes" })
@Slf4j
public final class Netweaver extends DirectoryWatcher{//TODO reload parent mustache when partial is changed
	public final static ObjectMapper mapper = new ObjectMapper()
			.setVisibility(PropertyAccessor.CREATOR, Visibility.PUBLIC_ONLY)
			.setVisibility(PropertyAccessor.SETTER, Visibility.PUBLIC_ONLY)
			.setVisibility(PropertyAccessor.GETTER, Visibility.PUBLIC_ONLY)
			.setVisibility(PropertyAccessor.IS_GETTER, Visibility.PUBLIC_ONLY)
			.setVisibility(PropertyAccessor.FIELD, Visibility.PUBLIC_ONLY)
			.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
			.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
			.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
			.registerModule(new SimpleModule("Netweaver").addSerializer(java.util.Date.class, new DateSerializer()));
	private static final ConcurrentHashMap<String, Validator> validators = new ConcurrentHashMap<>();
	
	public static final File resDir = new File("www/");

	private static Tika tika;
	private static MustacheFactory htmlMustacheFactory;
	private static MustacheFactory plaintextMustacheFactory;
	private static PebbleEngine pebbleEngine;

	private static final ConcurrentHashMap<Class<? extends Service>, Service> services = new ConcurrentHashMap<>();
	private static final HashMap<Class<? extends Service>, LinkedList<Class<? extends Service>>> dependencies = new HashMap<>();
	
	private static final ConcurrentHashMap<String, BaseController<? extends Session>> controllers = new ConcurrentHashMap<>();
	private static final ConcurrentHashMap<Class<BaseController<? extends Session>>, BaseController<? extends Session>> controllersByClass = new ConcurrentHashMap<>();
	
	private static final ConcurrentHashMap<String, Template> templates = new ConcurrentHashMap<>();

	private static DirectoryWatcher wwwWatcher;
	
	private static Reflections reflections;

	private Netweaver() throws IOException, UnsupportedOperationException {
		super(resDir);
	}
	
	@Getter
	private static volatile boolean isBootstrapped = false;
	
	public static final void bootstrap() throws Exception{
		bootstrap(null, null);
	}
	
	public static final void bootstrap(Class<? extends Session> sessionClass) throws Exception{
		bootstrap(sessionClass, null);
	}
	
	public static final <SessionType extends Session> void bootstrap(Class<SessionType> sessionClass, Class<? extends SessionManager<SessionType>> sessionManagerClass) throws Exception{
		if(isBootstrapped)throw new Exception("Netweaver already started.");
		
		log.debug("Resource dir: " + resDir.getAbsolutePath());
		
		long now = System.currentTimeMillis();
		
		tika =  new Tika();
		htmlMustacheFactory = new HtmlMustacheFactory();
		plaintextMustacheFactory = new PlainTextMustacheFactory();
		pebbleEngine = new PebbleEngine.Builder().cacheActive(false).strictVariables(true).build();

		ArrayList<Class<? extends Service>> classes = new ArrayList<>();
		
		reflections = new Reflections(new TypeAnnotationsScanner(), new SubTypesScanner(), new FieldAnnotationsScanner());
		for(Class<? extends Service> clazz : reflections.getSubTypesOf(Service.class)){
			if(!Modifier.isAbstract(clazz.getModifiers())){
				if(Server.class.isAssignableFrom(clazz) || SessionManager.class.isAssignableFrom(clazz))continue;

				boolean wasSet = false;
				for(int i = 0;i < classes.size();i++){
					if(classes.get(i).isAssignableFrom(clazz)){
						if(log.isDebugEnabled())log.debug("Replacing service " + classes.get(i).getName() + " with it's subclass " + clazz.getName() + "...");
						classes.set(i, clazz);
						wasSet = true;
						break;
					}
				}
				if(wasSet)continue;

				if(log.isDebugEnabled())log.debug("Adding service " + clazz.getName() + "...");
				classes.add(clazz);
			}
		}
		
		Class<? extends Service> smgrClass = sessionManagerClass;

		if(smgrClass == null){
			if(log.isInfoEnabled())log.info("Using default session manager " + SessionManager.class.getName() + ".");
			smgrClass =  SessionManager.class;
		}
		
		classes.add(smgrClass);
	
		if(sessionClass == null){
			if(log.isInfoEnabled())log.info("Using default session " + Session.class.getName() + ".");
			sessionClass = (Class<SessionType>) Session.class;
		}


		for(Class<? extends Service> clazz : classes){
			try {
				services.put(clazz, clazz.getDeclaredConstructor().newInstance());
			} catch (InstantiationException | IllegalAccessException | IllegalAccessError e) {
				throw new Exception("Could not instantiate service " + clazz.getName() + ". Please ensure that this service (class) has as default constructor (no parameters). You may put initialization code in its .start() method.", e);
			}
		}

		Server server = new Server(port, defaultUri);//TODO handle ssl context
		server.sessionManager = (SessionManager) services.get(smgrClass);
		if(server.sessionManager != null)server.sessionManager.sessionClass = sessionClass;
		services.put(server.getClass(), server);
		
		for(Class<? extends BaseController> clazz : reflections.getSubTypesOf(BaseController.class)){
			if(!Modifier.isAbstract(clazz.getModifiers())){
				try {
					BaseController handler = clazz.getDeclaredConstructor().newInstance();
					controllersByClass.put((Class<BaseController<? extends Session>>) clazz, handler);

					BaseController oldHandler = controllers.get(handler.getUri());

					if(handler.getSessionClass().isAssignableFrom(sessionClass)){
						if(oldHandler != null){
							if(oldHandler.getClass().isAssignableFrom(handler.getClass())){
								if(log.isInfoEnabled())log.info("Mapping " + handler.getClass().getName() + " to uri " + handler.getUri() + " as " + handler.getContentType() + "...");
								controllers.put(handler.getUri(),  handler);
							}else if(!handler.getClass().isAssignableFrom(oldHandler.getClass())){
								if(log.isErrorEnabled())log.error("Duplicate mappings found between " + oldHandler.getClass().getName() + " and " + handler.getClass().getName() + " for uri " + handler.getUri() + ". Not using either.");
								controllers.remove(handler.getUri());
							}		
						}else{
							if(log.isInfoEnabled())log.info("Mapping " + handler.getClass().getName() + " to uri " + handler.getUri() + " as " + handler.getContentType() + "...");
							controllers.put(handler.getUri(), handler);
						}
					}else{
						log.info("Ignoring " + clazz.getName() + ": invalid session type.");
					}
				} catch (InstantiationException | IllegalAccessException e) {
					throw new Exception("Could not instantiate " + clazz.getName() + ". Please ensure that this handler (class) has as default constructor (no parameters).", e);
				}
			}
		}

		inject(null);
		
		for(Field field : reflections.getFieldsAnnotatedWith(Inject.class)){
			if(Service.class.isAssignableFrom(field.getDeclaringClass()) && Service.class.isAssignableFrom(field.getType())){
				LinkedList<Class<? extends Service>> d = dependencies.get(field.getDeclaringClass());
				if(d == null){
					d = new LinkedList<Class<? extends Service>>();
					dependencies.put((Class<Service>)field.getDeclaringClass(), d);
				}
				d.add((Class<Service>)field.getType());
				
				if(log.isDebugEnabled())log.debug("Found dependancy: " + field.getDeclaringClass().getName() + " depends on " + field.getType().getName());
				
				d = dependencies.get(field.getType());
				if(log.isWarnEnabled() && d != null && d.contains(field.getDeclaringClass()))log.warn("Found mutually dependant services: " + field.getType().getName() + " and " + field.getDeclaringClass().getName() + ". This may cause one service to be used before it is started.");
			}
		}
		
		isBootstrapped = true;

		try {
			wwwWatcher = new Netweaver();
			new Thread(wwwWatcher).start();
		} catch (UnsupportedOperationException | IOException e) {
			if(log.isErrorEnabled())log.error("Could not register watch service for resource directory (" + resDir.getAbsolutePath() + "). Changes to templates will not be reloaded automatically.", e);
		}
		
		temp.clear();
		for(Service service : services.values()){
			startWithDependencies(service);
		}
		temp.clear();

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				if(wwwWatcher != null)wwwWatcher.close();
				for(Service service : services.values()){
					try{
						if(log.isDebugEnabled())log.debug("Stopping service " + service.getClass().getName() + "...");
						service.stop();
					}catch(Throwable t){
						if(log.isErrorEnabled())log.error("Could not stop service " + service.getClass().getName() + ".", t);
					}
				}
			}
		});
		
		if(log.isDebugEnabled())log.debug("Took " + (System.currentTimeMillis() - now) + "ms to bootstrap.");
	}
	
	public static void validate(Object obj) throws ValidationException{
		validate(obj, Netweaver.locale);
	}
	
	public static void validate(Object obj, Locale l) throws ValidationException{
		Set<ConstraintViolation<Object>> constraintViolations = getValidator(l).validate(obj);
		if(!constraintViolations.isEmpty()){
			ConstraintViolation<?> violation = constraintViolations.iterator().next();
			throw new ValidationException(violation.getPropertyPath().toString() + " " + violation.getMessage());
		}
	}
	
	public static ValidationException[] getValidationExceptions(Object obj, Locale l){
		Set<ConstraintViolation<Object>> constraintViolations = getValidator(l).validate(obj);
		ValidationException[] constraints = new ValidationException[constraintViolations.size()];
		int i = 0;
		for(ConstraintViolation<?> violation : constraintViolations){
			constraints[i++] = new ValidationException(violation.getPropertyPath().toString() + " " + violation.getMessage());
		}return constraints;
	}
	
	public static Validator getValidator(final Locale l){
		Validator validator = validators.get(l.getLanguage());
		if(validator == null){
			validator = Validation.byDefaultProvider().configure().messageInterpolator(new ResourceBundleMessageInterpolator(){
				@Override
				public String interpolate(final String messageTemplate, final Context context) {
					return interpolate(messageTemplate, context, l);

				}

				@Override
				public String interpolate(final String messageTemplate, final Context context, final Locale locale) {
					return super.interpolate(messageTemplate, context, l);
				}

			}).buildValidatorFactory().getValidator();
			validators.put(l.getLanguage(), validator);
		}return validator;
	}
	
	public static <T> T fromJson(String json, @NonNull Class<T> c) throws JsonException{
		try {
			return mapper.readValue(json, c);
		}catch (Throwable t) {
			if(log.isErrorEnabled())log.error("Could not parse JSON.", t);
			throw new JsonException(t.getMessage().split("\n")[0]);
		}
	}
	
	public static <T> T fromJson(String json, @NonNull TypeReference<T> c) throws JsonException{
		try {
			return mapper.readValue(json, c);
		}catch (Throwable t) {
			if(log.isErrorEnabled())log.error("Could not parse JSON.", t);
			throw new JsonException(t.getMessage().split("\n")[0]);
		}
	}
	
	public static String toJson(Object obj){
		try {
			return mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			if(log.isWarnEnabled())log.warn("Could not map POJO to JSON", e);
			throw new IllegalAccessError(e.getMessage());
		}
	}
	
	public static final int port;
	public static final String defaultUri;

	public static final Charset charset;
	public static final Locale defaultLocale = Locale.getDefault();

	public static final boolean isMetered;
	public static final int coreCount, spindleCount;

	public static final String templateStart, templateEnd;

	public static Locale locale;

	public static final Template getTemplate(String uri){
		return templates.get(uri);
	}

	public static final <S extends Service> S getService(Class<S> serviceClass){
		return (S) services.get(serviceClass);
	}
	
	public static final <S extends Service> S setService(@NonNull S service) throws Exception{
		if(!isBootstrapped)throw new IllegalAccessError("Server not boostrapped.");
		Service old = services.put(service.getClass(), service);
		if(service == old)return service;
		
		inject(service);
		
		if(old != null)old.stop();
		service.start();
		
		return (S) old;
	}
	
	public static final <T extends BaseController> T getHandler(Class<T> handlerClass){
		return (T) controllersByClass.get(handlerClass);
	}
	
	public static final BaseController<? extends Session> getHandler(String uri){
		return controllers.get(uri);
	}
	
	public static final Collection<BaseController<? extends Session>> getHandlers(){
		return controllers.values();
	}
	
	private static final HashSet<Service> temp = new HashSet<>();
	private static final void startWithDependencies(Service service) throws Exception{
		if(temp.contains(service))return;
		LinkedList<Class<? extends Service>> dependencies = Netweaver.dependencies.get(service.getClass());
		if(dependencies == null){
			temp.add(service);
			if(log.isDebugEnabled())log.debug("Starting " + service.getClass().getName() + "...");
			try{
				service.start();
			}catch(Throwable e){
				throw new Exception("Could not start service.", e);
			}
		}else{
			Iterator<Class<? extends Service>> serviceIterator = dependencies.iterator();
			while(serviceIterator.hasNext()){
				Class<? extends Service> s = serviceIterator.next();
				serviceIterator.remove();
				startWithDependencies(services.get(s));
			}
			if(!temp.contains(service)){
				temp.add(service);
				if(log.isDebugEnabled())log.debug("Starting " + service.getClass().getName() + "...");
				try{
					service.start();
				}catch(Throwable e){
					throw new Exception("Could not start service.", e);
				}
			}
		}
	}
	
	private static final void inject(Service toInject) throws Exception{
		for(Field field : reflections.getFieldsAnnotatedWith(Inject.class)){
			if(toInject != null && !toInject.getClass().equals(field.getType()))continue;
			try {
				Object o = Service.class.isAssignableFrom(field.getType()) ? services.get(field.getType()) : BaseController.class.isAssignableFrom(field.getType()) ? controllersByClass.get(field.getType()) : null;
				if(o == null)throw new IllegalArgumentException("Service/RequestHandler does not exist or is not a non-abstract subclass of either " + Service.class.getName() + " or " + BaseController.class.getName() + ".");

				if(Modifier.isStatic(field.getModifiers())){
					if(log.isDebugEnabled())log.debug("Injecting " + o.getClass().getName() + " into field " + field.getDeclaringClass().getName() + "." + field.getName() + "...");
					field.setAccessible(true);
					field.set(null, o);
				}else{
					throw new IllegalAccessException("Field must be static.");
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				throw new Exception("Failed to inject Service/RequestHandler " + field.getType().getName() + " into field " + field.getDeclaringClass().getName() + "." + field.getName() + ".", e);
			}
		}
	}

	@Override
	protected void onCreate(File file) {
		if(file.isHidden() || file.getName().startsWith(".")){
			if(log.isInfoEnabled())log.info("Ignoring file " + file.getAbsolutePath());
			return;
		}
		String uri = file.getAbsolutePath().length() > Netweaver.resDir.getAbsolutePath().length() ? file.getAbsolutePath().substring(resDir.getAbsolutePath().length() + 1).replace("\\", "/") : "";
		
		if(uri.endsWith(".pebble")){
			uri = uri.substring(0, uri.length() - ".pebble".length());
		}else if(uri.endsWith(".mustache")){
			uri = uri.substring(0, uri.length() - ".mustache".length());
		}
		
		if(controllers.get(uri) == null){
			File parentFile = file.getParentFile();
			String parentUri = parentFile != null && parentFile.getAbsolutePath().length() > Netweaver.resDir.getAbsolutePath().length() ? parentFile.getAbsolutePath().substring(Netweaver.resDir.getAbsolutePath().length() + 1).replace("\\", "/") : "";
			BaseController handler = controllers.get(parentUri);
			
			while(handler == null && parentUri.length() > 0){
				parentFile = parentFile.getParentFile();
				parentUri = parentFile != null && parentFile.getAbsolutePath().length() > Netweaver.resDir.getAbsolutePath().length() ? parentFile.getAbsolutePath().substring(Netweaver.resDir.getAbsolutePath().length() + 1).replace("\\", "/") : "";
				handler = controllers.get(parentUri);
			}
			
			if(handler == null){
				if(log.isDebugEnabled())log.debug("Could not map child uri " + uri + ": No parent handler mapped (" + parentUri + ").");
				return;
			}
			
			try {
				controllers.put(uri, handler.clone(uri));
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				if(log.isWarnEnabled())log.warn("Could not clone RequestHandler. Using parent instead (getUri() in child will not reflect the actual request uri).", e);
				controllers.put(uri, handler);
			}	
		}
		if(file.isFile())onModify(file);
	}

	@Override
	protected void onModify(File file) {
		String uri = file.getAbsolutePath().length() > Netweaver.resDir.getAbsolutePath().length() ? file.getAbsolutePath().substring(Netweaver.resDir.getAbsolutePath().length() + 1).replace("\\", "/") : "";
		
		TemplateType templateType = TemplateType.NONE;
		if(uri.endsWith(".pebble")){
			templateType = TemplateType.PEBBLE;
			uri = uri.substring(0, uri.length() - ".pebble".length());
		}else if(uri.endsWith(".mustache")){
			templateType = TemplateType.MUSTACHE;
			uri = uri.substring(0, uri.length() - ".mustache".length());
		}
		
		String contentType;
		try {
			contentType = tika.detect(file);
		} catch (IOException e) {
			contentType = tika.detect(uri);
		}

		if(contentType.contains("text/"))contentType += "; charset=UTF-8";

		BaseController handler = controllers.get(uri);
		if(handler == null){
			if(log.isDebugEnabled())log.debug("Could not modify " + uri + ": No handler mapped.");
			return;
		}
		
		handler.setContentType(contentType);

		if(templateType != TemplateType.NONE){
			if(log.isInfoEnabled())log.info("caching " + uri + " as " + contentType + "...");
			try{
				switch(templateType){
				case MUSTACHE:
					final Mustache m = contentType.contains("html") ? htmlMustacheFactory.compile(file.getAbsolutePath()) : plaintextMustacheFactory.compile(file.getAbsolutePath());
					final Template mt = new Template(){
						
						private final Mustache mustache = m;
						
						@Override
						public String evaluate(Header h, Session s, Object r, Locale l) throws Exception {
							StringWriter writer = new StringWriter();
							mustache.execute(writer, new Object[]{h, s, r}).flush();
							return writer.toString();
						}

						@Override
						public TemplateType getTemplateType() {
							return TemplateType.MUSTACHE;
						}
						
					};
					
					handler.setTemplate(mt);
					templates.put(uri, mt);
					break;
				case PEBBLE:
					final PebbleTemplate p = pebbleEngine.getTemplate(file.getAbsolutePath());
					final Template pt = new Template(){

						private final PebbleTemplate pebble = p;
						
						@Override
						public String evaluate(Header h, Session s, Object r, Locale l) throws Exception {
							StringWriter writer = new StringWriter();
							Map<String, Object> context = new HashMap<>(2, 1f);
							context.put("h", h);
							context.put("r", r);
							context.put("s", s);
							pebble.evaluate(writer, context, l);
							return writer.toString();
						}

						@Override
						public TemplateType getTemplateType() {
							return TemplateType.PEBBLE;
						}
						
					};
					
					handler.setTemplate(pt);
					templates.put(uri, pt);
					break;
				default:
					if(log.isWarnEnabled())log.warn("Unimplemented Template type: " + templateType.toString());
					break;
				}
			}catch(Exception e){
				if(log.isErrorEnabled())log.error("Could not compile template.", e);
			}
		}
	}

	@Override
	protected void onDelete(File file) {
		String uri = file.getAbsolutePath().length() > Netweaver.resDir.getAbsolutePath().length() ? file.getAbsolutePath().substring(Netweaver.resDir.getAbsolutePath().length() + 1).replace("\\", "/") : "";
		templates.remove(uri);
		BaseController handler = controllers.get(uri);
		if(handler == null){
			if(log.isDebugEnabled())log.debug("Could not remove " + uri + " handler: No handler mapped.");
			return;
		}
		if(handler.isClone() && controllers.remove(uri) != null && log.isInfoEnabled())log.info("Removed " + uri + ".");
	}

	static{		
		File configFile = new File("config");
		if(!configFile.isDirectory())configFile.mkdir();
		configFile = new File("config/netweaver");

		Properties config = new Properties();

		if(configFile.isFile()){
			try {
				config.load(new BufferedReader(new FileReader(configFile)));
			} catch (IOException e1) {
				config.clear();
			}
		}

		if(config.isEmpty()){
			config.setProperty("coreCount", String.valueOf(Runtime.getRuntime().availableProcessors()));
			config.setProperty("spindleCount", String.valueOf(1));
			config.setProperty("isMetered", String.valueOf(true));
			config.setProperty("charset", "UTF-8");

			config.setProperty("templateStart", "{{");
			config.setProperty("templateEnd", "}}");

			config.setProperty("locale", "en-us");
			
			config.setProperty("port", "80");
			config.setProperty("defaultUri", "index");

			try {
				config.store(new BufferedWriter(new FileWriter(configFile)), "Netweaver configuration file.");
			} catch (IOException e) {}
		}

		Charset temp = null;
		try{
			temp = Charset.forName(config.getProperty("charset", "UTF-8"));
		}catch(Throwable e){
			if(log.isWarnEnabled())log.warn("Could not load charset: " + e.getLocalizedMessage());
			temp = Charset.defaultCharset();
		}

		coreCount = Integer.parseInt(config.getProperty("coreCount", String.valueOf(Runtime.getRuntime().availableProcessors())));
		spindleCount = Integer.parseInt(config.getProperty("spindleCount", String.valueOf(1)));
		isMetered = Boolean.parseBoolean(config.getProperty("isMetered", String.valueOf(true)));
		charset = temp;

		templateStart = config.getProperty("templateStart", "{{");
		templateEnd = config.getProperty("templateEnd", "}}");

		locale = Locale.forLanguageTag(config.getProperty("locale", "en-us"));
		
		port = Integer.parseInt(config.getProperty("port", "80"));
		defaultUri = config.getProperty("defaultUri", "index");

		if(coreCount < 1 || spindleCount < 1)throw new RuntimeException("Invalid spindle/core count (must both be greater than 0).");
	}
}
