package netweaver.server;

import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import netweaver.client.PartialCallback;
import netweaver.error.ServiceUnavailableException;
import netweaver.transport.ErrorMessage;
import netweaver.transport.Header;
import netweaver.util.Queue;

@Slf4j
public class Session{
	@Getter
	private String uid = "";
	private Channel channel = null;
	
	@Getter
	private SessionManager<?> sessionManager = null;
	
	@Getter
	private volatile boolean isDestroyed = false;
	
	private Queue<PendingRequest> pending = null;
	private ConcurrentHashMap<String, PartialCallback> defaultCallbacks = null;
	
	private Locale preferredLocale = Netweaver.locale;
	
	@Getter
	private long lastAccessTime = 0;
	
	@RequiredArgsConstructor
	private final class PendingRequest{
		final String uri;
		final String message;
		final boolean isError;
		final PartialCallback callback;
		volatile boolean isWritten = false;
		volatile boolean isHandled = false;
	}
	
	public void init(String uid, SessionManager<?> sgrp){
		if(this.uid.length() != 0)throw new IllegalAccessError("Session already initialized.");
		if(log.isInfoEnabled())log.info("session " + uid + " initialized.");
		this.uid = uid == null ? "" : uid;
		this.sessionManager = sgrp;
	}
	
	public void init(String uid){
		init(uid, null);
	}

	public void setChannel(@NonNull Channel channel) {
		if(this.channel == channel)return;
		if(log.isInfoEnabled()){
			if(this.channel != null)log.info(channel.remoteAddress().toString() + " (" + uid + ") re-connected.");
			else log.info(channel.remoteAddress().toString() + " (" + uid + ") connected.");
		}
		this.channel = channel;
		channel.closeFuture().addListener(new ChannelFutureListener() {
			
			@Override
			public void operationComplete(ChannelFuture future) throws Exception {
				if(future.channel() == Session.this.channel){
					Session.this.channel = null;
					if(log.isInfoEnabled())log.info(future.channel().remoteAddress().toString() + " (" + uid + ") disconnected.");
				}
			}
		});
	}

	public Channel getChannel() {
		return channel != null && channel.isOpen() ? channel : null;
	}

	public ChannelFuture write(@NonNull String uri, String message, boolean isError) throws ServiceUnavailableException {
		Channel channel = getChannel();
		if(channel == null)throw new ServiceUnavailableException();
		Header header = new Header(uri, message, isError);
		header.setUid(uid);
		header.setLang(preferredLocale);
		return channel.writeAndFlush(header);
	}
	
	public ChannelFuture write(String uri, @NonNull Object message) throws ServiceUnavailableException {
		return write(uri, Netweaver.toJson(message), message instanceof ErrorMessage);
	}

	public void request(@NonNull String uri, String message, boolean isError, @NonNull final PartialCallback callback){
		if(pending == null)pending = new Queue<>();
		
		final PendingRequest request = new PendingRequest(uri, message, isError, callback);
		synchronized(pending){
			pending.push(request);
		}
		
		invokeRequest();
	}
	
	public void request(String uri, @NonNull Object message, PartialCallback callback){
		request(uri, Netweaver.toJson(message), message instanceof ErrorMessage, callback);
	}
	
	private final void invokeRequest(){
		PendingRequest current;
		synchronized (pending){
			current = pending.peek();
			while(current != null && current.isHandled){
				pending.pop();
				current = pending.peek();
			}
		}
		
		try {
			if(current == null || current.isWritten)return;
			current.isWritten = true;
			
			final PendingRequest request = current;
			write(request.uri, request.message, request.isError).addListener(new ChannelFutureListener(){

				@Override
				public void operationComplete(ChannelFuture future)throws Exception {
					if(!future.isSuccess()){
						request.isHandled = true;
						try {
							Header header = new Header(request.uri, request.message, request.isError);
							header.setBody(new ErrorMessage(new ServiceUnavailableException()));
							header.setUid(uid);
							header.setLang(preferredLocale);
							request.callback.handleResponse(header);
						} catch (Throwable t) {
							log.error("Error handling unsuccessfull write callback.", t);
						}
					}else{
						future.channel().eventLoop().schedule(new Runnable(){
							
							@Override
							public void run() {
								if(!request.isHandled){
									request.isHandled = true;
									try {
										Header header = new Header(request.uri, request.message, request.isError);
										header.setBody(new ErrorMessage(new ServiceUnavailableException()));
										header.setUid(uid);
										header.setLang(preferredLocale);
										request.callback.handleResponse(header);
									} catch (Throwable t) {
										log.error("Error handling timeout callback.", t);
									}
								}
							}
							
						}, request.callback.getTimeout(), request.callback.getTimeUnit());
					}
				}
				
			});
		}catch (ServiceUnavailableException e) {
			synchronized(pending){
				while((current = pending.pop()) != null){
					try {
						if(!current.isHandled){
							current.isHandled = true;
							Header header = new Header(current.uri, current.message, current.isError);
							header.setBody(new ErrorMessage(new ServiceUnavailableException()));
							header.setUid(uid);
							header.setLang(preferredLocale);
							current.callback.handleResponse(header);
						}
					} catch (Throwable t) {
						log.error("Error handling unsuccessfull write callback.", t);
					}
				}
			}
		}
	}

	public boolean invokeHandler(@NonNull final String uri, @NonNull final String message, final boolean isError) {
		PartialCallback callback = null;
		if(pending != null){
			synchronized(pending){
				PendingRequest current;
				current = pending.peek();
				while(current != null && current.isHandled){
					pending.pop();
					current = pending.peek();
				}
				
				if(current != null && current.uri.equals(uri)){
					current.isHandled = true;
					pending.pop();
					callback = current.callback;
				}
			}
		}
		
		if(callback == null && (defaultCallbacks == null || (callback = defaultCallbacks.get(uri)) == null))return false;
		
		if(channel != null){
			final PartialCallback c = callback;
			channel.eventLoop().execute(new Runnable(){

				@Override
				public void run() {
					try{
						Header header = new Header(uri, message, isError);
						header.setUid(uid);
						header.setLang(preferredLocale);
						c.handleResponse(header);
					}catch(Throwable t){
						log.error("Error handling callback.", t);
					}
				}
				
			});
		}else{
			try{
				Header header = new Header(uri, message, isError);
				header.setUid(uid);
				header.setLang(preferredLocale);
				callback.handleResponse(header);
			}catch(Throwable t){
				log.error("Error handling callback.", t);
			}
		}
		
		invokeRequest();
		return true;
	}
	
	public boolean invokeHandler(String uri, @NonNull Object message) {
		return invokeHandler(uri, Netweaver.toJson(message), message instanceof ErrorMessage);
	}
	
	public void setDefaultCallback(@NonNull String uri, @NonNull PartialCallback callback) {
		if(defaultCallbacks == null)defaultCallbacks = new ConcurrentHashMap<>();
		defaultCallbacks.put(uri, callback);
	}
	
	public PartialCallback getCallback(String uri) {
		PartialCallback callback = null;
		if(pending != null){
			synchronized(pending){
				PendingRequest current;
				current = pending.peek();
				while(current != null && current.isHandled){
					pending.pop();
					current = pending.peek();
				}
				if(current != null && current.uri.equals(uri))callback = current.callback;
			}
		}
		if(callback == null && defaultCallbacks != null)callback = defaultCallbacks.get(uri);
		return callback;
	}
	
	public PartialCallback getDefaultCallback(@NonNull String uri) {
		if(defaultCallbacks == null)return null;
		return defaultCallbacks.get(uri);
	}

	public void updateLastAccessTime() {
		this.lastAccessTime = System.currentTimeMillis();
	}

	public void setPreferredLocale(Locale l) {
		this.preferredLocale = l == null ? Netweaver.locale : l;
	}

	public Locale getPreferredLocale() {
		return preferredLocale == null ? Netweaver.locale : preferredLocale;
	}

	public boolean closeChannel() {
		Channel channel = getChannel();
		if(channel == null)return false;
		
		channel.close();
		this.channel = null;
		return true;
	}
	
	/**
	 * Does not close channel.
	 * 
	 * returns true if session was not already destroyed.
	 */
	public boolean destroy(){
		if(isDestroyed)return false;
		isDestroyed = true;
		if(log.isDebugEnabled())log.debug(uid + " destroyed.");
		
		if(sessionManager != null)sessionManager.remove(getUid());
		
		return false;
	}

	public boolean isOpen() {
		return getChannel() != null;
	}
}
