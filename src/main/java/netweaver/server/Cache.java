package netweaver.server;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import netweaver.interfaces.Service;
import netweaver.metrics.TimerMetric;

@Slf4j
public abstract class Cache implements Service, Runnable{
	@JsonIgnore
	private ExecutorService executor;
	
	@JsonIgnore
	private final Object syncLock = new Object();
	private final AtomicInteger version = new AtomicInteger(0);

	@JsonIgnore @Getter
	private volatile boolean isRunning = false;
	
	@JsonIgnore
	private volatile boolean isUpdating = false;
	@JsonIgnore
	private volatile String payload;

	private final void setUpdating(){
		waitForUpdate();
		isUpdating = true;
	}

	private final void notifyUpdated(){
		isUpdating = false;
		synchronized(syncLock){
			syncLock.notifyAll();
		}
	}

	protected void waitForUpdate(){
		while(isUpdating){
			synchronized(syncLock){
				try {
					syncLock.wait();
				} catch (InterruptedException e) {}
			}
		}
	}

	public final void reload(){
		executor.execute(this);
	}
	
	@Override
	public final void run(){
		if(log.isInfoEnabled())log.info("reloading " + getClass().getSimpleName() + "...");
		TimerMetric.Promise promise = Netweaver.isMetered ? TimerMetric.start(getClass().getSimpleName() + ".reload()") : null;
		setUpdating();
		try{
			payload = null;
			_reload();
			version.incrementAndGet();
		}catch(Exception e){
			if(promise != null)promise.setError(true);
			if(log.isErrorEnabled())log.error("Error reloading cache.", e);
		}finally{
			notifyUpdated();
			if(promise != null)TimerMetric.finish(promise);
		}
	}

	public final int getVersion(){
		return version.get();
	}
	
	@Override
	public void start() {
		if(executor != null)throw new IllegalAccessError(getClass().getName() + " already running.");
		executor = Executors.newFixedThreadPool(1);
		reload();
	}

	@Override
	public void stop() {
		if(executor != null)executor.shutdown();
	}

	protected abstract void _reload() throws Exception;
}
