package netweaver.server.netty;

import java.nio.CharBuffer;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.util.ReferenceCountUtil;
import netweaver.error.BadRequestException;
import netweaver.server.Netweaver;
import netweaver.transport.Header;

@Sharable
public class TcpCodec extends ChannelDuplexHandler{
	private static TcpCodec instance;
	
	public static TcpCodec get(){
		return instance == null ? instance = new TcpCodec() : instance;
	}
	
	protected TcpCodec(){}
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object _msg) throws Exception {
		try{
			String msg = ((ByteBuf)_msg).toString(Netweaver.charset);
			
			int headerEnd = msg.indexOf("}{") + 1;
			if(headerEnd < 1)throw new BadRequestException();//TODO handle more gracefully
			Header header = Netweaver.fromJson(msg.substring(0, headerEnd), Header.class);
			if(header.getType() == null)header.setType(Header.Type.TCP);
			
			header.setBody(msg.substring(headerEnd), header.isError());
			
			ctx.fireChannelRead(header);
		}finally{
			ReferenceCountUtil.release(_msg);
		}
	}

	@Override
	public void write(ChannelHandlerContext ctx, Object _msg, ChannelPromise promise) throws Exception {
		try{
			Header msg = (Header) _msg;
			ctx.write(ByteBufUtil.encodeString(ctx.alloc(), CharBuffer.wrap(Netweaver.toJson(msg) + msg.getBody() + "\n"), Netweaver.charset));
		}finally{
			ReferenceCountUtil.release(_msg);
		}
	}
}
