package netweaver.server.netty;

import java.util.List;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageCodec;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import netweaver.error.BadRequestException;
import netweaver.server.Netweaver;
import netweaver.transport.Header;

@Sharable
public class WebSocketCodec extends MessageToMessageCodec<WebSocketFrame, Header>{
	private static WebSocketCodec instance = null;
	public static final WebSocketCodec get(){
		return instance != null ? instance : (instance = new WebSocketCodec());
	}
	
	private WebSocketCodec() {}
	
	@Override
	protected void decode(ChannelHandlerContext ctx, WebSocketFrame _msg, List<Object> out) throws Exception {
		String msg;
		boolean isBinary;
		if(isBinary = _msg instanceof BinaryWebSocketFrame)msg = ((BinaryWebSocketFrame)_msg).content().toString(Netweaver.charset);
		else msg = ((TextWebSocketFrame)_msg).text();
		
		int headerEnd = msg.indexOf("}{") + 1;
		if(headerEnd < 1)throw new BadRequestException();//TODO handle more gracefully
		Header header = Netweaver.fromJson(msg.substring(0, headerEnd), Header.class);
		if(header.getType() == null)header.setType(Header.Type.WEBSOCKET);
		header.setBinary(isBinary);
		
		header.setBody(msg.substring(headerEnd), header.isError());
		
		out.add(header);
	}
	
	@Override
	protected void encode(ChannelHandlerContext ctx, Header msg, List<Object> out) throws Exception {
		if(msg.isBinary())out.add(new BinaryWebSocketFrame(Unpooled.wrappedBuffer((Netweaver.toJson(msg) + msg.getBody()).getBytes(Netweaver.charset))));
		else out.add(new TextWebSocketFrame(Netweaver.toJson(msg) + msg.getBody()));
	}

}