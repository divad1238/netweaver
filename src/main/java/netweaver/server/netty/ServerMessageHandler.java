package netweaver.server.netty;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import netweaver.error.InternalServerException;
import netweaver.error.InvalidUriException;
import netweaver.error.UserException;
import netweaver.server.Netweaver;
import netweaver.server.Server;
import netweaver.server.Session;
import netweaver.server.controller.BaseController;
import netweaver.transport.ErrorMessage;
import netweaver.transport.Header;

@Slf4j
@AllArgsConstructor
@RequiredArgsConstructor
public class ServerMessageHandler extends SimpleChannelInboundHandler<Header>{
	private final Server<?> server;

	private Session session;

	@SuppressWarnings("unchecked")
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Header msg)throws Exception {
		String uid = msg.getUid();
		
		Object response = null;
		try{
			if(session == null || session.isDestroyed()){
				session = server.getSessionManager().get(uid);
				uid = session.getUid();
			}else if(!session.getUid().equals(uid)){
				session.destroy();
				session = server.getSessionManager().get(uid);
				uid = session.getUid();
			}

			session.updateLastAccessTime();
			session.setChannel(ctx.channel());
			
			if(session.invokeHandler(msg.getUri(), msg.getBody(), msg.isError()))return;

			@SuppressWarnings("rawtypes")
			final BaseController handler = Netweaver.getHandler(msg.getUri().length() > 0 && msg.getUri().charAt(0) == '/' ? msg.getUri().substring(1) : msg.getUri());

			if(handler == null)throw new InvalidUriException(msg.getUri());
			
			if(msg.getLang() != Netweaver.locale)session.setPreferredLocale(msg.getLang());
			else msg.setLang(session.getPreferredLocale());
			
			if(log.isDebugEnabled())log.debug(uid + " -> " + msg.getType().toString() + " " + msg.getUri() + " " + msg.getBody().substring(0, Math.min(msg.getBody().length(), 500)));
			response = handler.handleRequest(msg, session);
		}catch(InternalServerException e){
			log.error("Unhandled internal server error.", e.getCause());
			response = new ErrorMessage(e);
		}catch(UserException e){
			response = new ErrorMessage(e);
		}catch(Throwable e){
			log.error("Unhandled internal server error.", e);
			response = new ErrorMessage(new InternalServerException(e));
		}finally{
			if(response == null)return;
			msg.setUid(uid);
			msg.setBody(response);
			msg.setTag(response);
			
			ctx.writeAndFlush(msg);
			if(log.isDebugEnabled())log.debug(uid + " <- " + msg.getType().toString() + " " + msg.getUri() + " " + msg.getBody().substring(0, Math.min(msg.getBody().length(), 500)));
		}
	}
}
