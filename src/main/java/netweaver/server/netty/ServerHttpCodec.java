package netweaver.server.netty;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPromise;
import io.netty.channel.DefaultFileRegion;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpChunkedInput;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpUtil;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.handler.codec.http.QueryStringDecoder;
import io.netty.handler.codec.http.cookie.Cookie;
import io.netty.handler.codec.http.cookie.ServerCookieDecoder;
import io.netty.handler.codec.http.cookie.ServerCookieEncoder;
import io.netty.handler.codec.http.multipart.Attribute;
import io.netty.handler.codec.http.multipart.FileUpload;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.stream.ChunkedFile;
import io.netty.util.ReferenceCountUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import netweaver.error.BadRequestException;
import netweaver.error.InternalServerException;
import netweaver.error.InvalidUriException;
import netweaver.server.Netweaver;
import netweaver.server.Server;
import netweaver.server.Session;
import netweaver.server.controller.BaseController;
import netweaver.template.Template;
import netweaver.transport.ErrorMessage;
import netweaver.transport.Header;
import netweaver.util.Util;

@Slf4j
@RequiredArgsConstructor
public class ServerHttpCodec extends ChannelDuplexHandler{
	private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
	private static final Pattern urlSanitizer = Pattern.compile("[//]+");

	static{
		dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
	}

	private final Server<?> server;
	private final String webSocketPath;

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {//TODO use StringBuilder instead of traditional concatenation for performance reasons
		try{
			FullHttpRequest request = (FullHttpRequest) msg;

			if(!request.decoderResult().isSuccess())throw new BadRequestException();

			QueryStringDecoder query = new QueryStringDecoder(request.uri(),Netweaver.charset);
			String uri = QueryStringDecoder.decodeComponent(urlSanitizer.matcher(query.path()).replaceAll("/").substring(1), Netweaver.charset), uid = null, lang = null;
			if(uri.length() == 0)uri = server.getDefaultUri();
			else if(new File(Netweaver.resDir.getAbsolutePath() + File.separator + uri).isDirectory())uri += (uri.charAt(uri.length() - 1) == '/' ? "" : "/") + server.getDefaultUri();

			if(uri.equals(webSocketPath)){
				ChannelPipeline p = ctx.pipeline();
				p.addAfter(ctx.name(), null, WebSocketCodec.get());
				p.addAfter(ctx.name(), null, new WebSocketServerProtocolHandler(webSocketPath));
				p.remove(this);
				ctx.fireChannelRead(ReferenceCountUtil.retain(msg));
				return;
			}

			String temp = request.headers().getAsString(HttpHeaderNames.COOKIE), json = "";
			if(temp != null && !temp.isEmpty()){
				Set<Cookie> cookies = ServerCookieDecoder.STRICT.decode(temp);
				for(Cookie cookie : cookies){
					switch(cookie.name()){
					case "uid":
						uid = StringEscapeUtils.escapeJson(cookie.value());
						break;
					case "lang":
						lang = StringEscapeUtils.escapeJson(cookie.value());
						break;
					default:
						/*if(Util.isNumeric(cookie.value()) != Util.NumType.NONE){
							json += ",\"" + StringEscapeUtils.escapeJson(cookie.name()) + "\":" + StringEscapeUtils.escapeJson(cookie.value());
						}else{
							json += ",\"" + StringEscapeUtils.escapeJson(cookie.name()) + "\":\"" + StringEscapeUtils.escapeJson(cookie.value()) + "\"";
						}*/
					}
				}
			}

			for(Entry<String, List<String>> entry : query.parameters().entrySet()){
				if(entry.getValue() != null && !entry.getValue().isEmpty()){
					if(Util.isNumeric(entry.getValue().get(0)) != Util.NumType.NONE){
						json += ",\"" + StringEscapeUtils.escapeJson(entry.getKey()) + "\":" + StringEscapeUtils.escapeJson(entry.getValue().get(0));
					}else{
						json += ",\"" + StringEscapeUtils.escapeJson(entry.getKey()) + "\":\"" + StringEscapeUtils.escapeJson(entry.getValue().get(0)) + "\"";
					}
				}
			}

			temp = request.headers().getAsString(HttpHeaderNames.CONTENT_TYPE);
			if(temp != null && !temp.isEmpty()){
				if(temp.contains("json")){
					temp = request.content().toString(Netweaver.charset);
					if(temp != null && !temp.isEmpty())json += "," + temp.substring(1, temp.length() - 1);
				}else if(temp.contains("x-www-form-urlencoded") || temp.contains("multipart")){//POST
					HttpPostRequestDecoder decoder = new HttpPostRequestDecoder(request);
					try{
						for(InterfaceHttpData data : decoder.getBodyHttpDatas()){
							switch(data.getHttpDataType()){
							case Attribute:
								Attribute a = (Attribute) data;
								if(Util.isNumeric(a.getValue()) != Util.NumType.NONE){
									json += ",\"" + StringEscapeUtils.escapeJson(a.getName()) + "\":" + StringEscapeUtils.escapeJson(a.getValue());
								}else{
									json += ",\"" + StringEscapeUtils.escapeJson(a.getName()) + "\":\"" + StringEscapeUtils.escapeJson(a.getValue()) + "\"";
								}
								break;
							case FileUpload:
								FileUpload file = (FileUpload) data;
								json += ",\"" + file.getName() + "\":{" +
										"\"fileName\":\"" + StringEscapeUtils.escapeJson(file.getFilename()) + 
										"\",\"contentType\":\"" + StringEscapeUtils.escapeJson(file.getContentType()) + 
										"\",\"data\":" + Netweaver.mapper.writeValueAsString(file.content().array()) + "}";
								break;
							case InternalAttribute:
								throw new Error("Unexpected post body type.");
							default:
								throw new Error("Unexpected post body type.");
							}
						}
					}finally{
						decoder.destroy();
					}
				}
			}

			if(lang == null)lang = request.headers().getAsString(HttpHeaderNames.ACCEPT_LANGUAGE);//TODO handle differently when header is parsed as json
			
			if(json.length() > 0)json = json.substring(1);
			
			Header header = new Header(uri, "{" + json + "}", false);
			
			header.setUid(uid == null ? "" : uid);
			header.setType(Header.Type.valueOf(request.method().name()));
			header.setIfModifiedSince(request.headers().getAsString(HttpHeaderNames.IF_MODIFIED_SINCE));
			header.setKeepAlive(HttpUtil.isKeepAlive(request));
			header.setLang(lang);

			ctx.fireChannelRead(header);
		}finally{
			ReferenceCountUtil.release(msg);
		}
	}

	@Override
	public void write(ChannelHandlerContext ctx, Object _msg, ChannelPromise promise) throws Exception {
		try{
			Header msg = (Header) _msg;
			HttpResponse response;
			Object part2 = null;
			String contentType;
			String uri = msg.getUri();
			HttpResponseStatus status = HttpResponseStatus.OK;

			if(msg.isError()){
				String key = (msg.getTag() != null ? (ErrorMessage) msg.getTag() : Netweaver.fromJson(msg.getBody(), ErrorMessage.class)).getKey();
				if(key.equals(InvalidUriException.class.getSimpleName())){
					status = HttpResponseStatus.NOT_FOUND;
				}else if(key.equals(BadRequestException.class.getSimpleName())){
					status = HttpResponseStatus.BAD_REQUEST;
				}else if(key.equals(InternalServerException.class.getSimpleName())){
					status = HttpResponseStatus.INTERNAL_SERVER_ERROR;
				}else{
					status = HttpResponseStatus.BAD_REQUEST;
				}
			}

			BaseController<?> handler = Netweaver.getHandler(uri);
			File file = new File(Netweaver.resDir.getAbsolutePath() + File.separator + uri);
			if((handler != null && handler.isTemplate()) || file.isFile()){
				contentType = handler == null ? "text/html" : handler.getContentType();
				if(handler != null && handler.isTemplate()){
					try{
						Template t = handler.getTemplate();
						Session s = server.getSessionManager().get(msg.getUid());
						response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status, Unpooled.wrappedBuffer(t.evaluate(msg, s, msg.getTag(), s.getPreferredLocale()).getBytes(Netweaver.charset)));
						HttpUtil.setContentLength(response, ((DefaultFullHttpResponse)response).content().readableBytes());
					}catch(Throwable t){
						if(log.isErrorEnabled())log.error("Error evaluating template.", t);
						contentType = "application/json; charset=UTF-8";
						response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status = HttpResponseStatus.INTERNAL_SERVER_ERROR, Unpooled.wrappedBuffer(Netweaver.toJson(new ErrorMessage(t.getClass().getSimpleName(), t.getMessage())).getBytes(Netweaver.charset)));
						HttpUtil.setContentLength(response, ((DefaultFullHttpResponse)response).content().readableBytes());
					}
				}else{
					boolean modifiedSince = true;
					if (msg.getIfModifiedSince() != null && msg.getIfModifiedSince().isEmpty()) {
						try {
							Date ifModifiedSinceDate = dateFormatter.parse(msg.getIfModifiedSince());
							modifiedSince = ifModifiedSinceDate != null && ifModifiedSinceDate.getTime() == file.lastModified();
						} catch (ParseException e) {}
						modifiedSince = true;//TODO fix caching problem
					}

					Calendar time = new GregorianCalendar();

					if(!modifiedSince){
						status = HttpResponseStatus.NOT_MODIFIED;
						response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status);
						response.headers().set(HttpHeaderNames.DATE, dateFormatter.format(time.getTime()));
					}else{
						try{
							RandomAccessFile raf = new RandomAccessFile(file, "r");
							long fileLen = raf.length();
							response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, status);
							HttpUtil.setContentLength(response, fileLen);
							response.headers().set(HttpHeaderNames.DATE, dateFormatter.format(time.getTime()));

							time.add(Calendar.SECOND, 1800);//Cache for 30 min 
							//response.headers().set(HttpHeaderNames.EXPIRES, dateFormatter.format(time.getTime())); //TODO fix caching problem
							//response.headers().set(HttpHeaderNames.CACHE_CONTROL, "private, max-age=" + 1800);//30 min cache//TODO fix caching problem
							//response.headers().set(HttpHeaderNames.LAST_MODIFIED, dateFormatter.format(new Date(file.lastModified())));//TODO fix caching problem

							if(ctx.pipeline().get(SslHandler.class) == null)part2 = new DefaultFileRegion(raf.getChannel(), 0, fileLen);
							else part2 = new HttpChunkedInput(new ChunkedFile(raf, 0, fileLen, 8192));
						} catch (FileNotFoundException ignore) {
							throw new InvalidUriException(uri);//should not happen
						}
					}
				}
			}else{
				contentType = "application/json; charset=UTF-8";
				response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, status, Unpooled.wrappedBuffer(msg.getBody().getBytes(Netweaver.charset)));
				HttpUtil.setContentLength(response, ((DefaultFullHttpResponse)response).content().readableBytes());
			}

			response.headers().add(HttpHeaderNames.SET_COOKIE, ServerCookieEncoder.STRICT.encode("uid", msg.getUid()));
			response.headers().set(HttpHeaderNames.CONTENT_TYPE, contentType);
			if(msg.isKeepAlive())response.headers().set(HttpHeaderNames.CONNECTION, HttpHeaderValues.KEEP_ALIVE);

			ChannelFuture f;

			if(part2 == null){
				f = ctx.write(response);
			}else if(part2 instanceof DefaultFileRegion){
				ctx.write(response);
				ctx.write(part2);
				f = ctx.write(LastHttpContent.EMPTY_LAST_CONTENT);
			}else if(part2 instanceof HttpChunkedInput){
				ctx.write(response);
				f = ctx.write(part2);
			}else throw new Exception("unknown response type.");

			if(!msg.isKeepAlive() || response.status() != HttpResponseStatus.OK){
				f.addListener(ChannelFutureListener.CLOSE);
			}
		}finally{
			ReferenceCountUtil.release(_msg);
		}
	}

	@Override 
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable t) throws Exception{
		log.error("Internal server error.", t);
		if(ctx.channel().isActive()){
			ctx.writeAndFlush(new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.INTERNAL_SERVER_ERROR)).addListener(ChannelFutureListener.CLOSE);
		}
	}
}
