package netweaver.server.netty;

import java.util.List;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.compression.ZlibCodecFactory;
import io.netty.handler.codec.compression.ZlibWrapper;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.cors.CorsConfigBuilder;
import io.netty.handler.codec.http.cors.CorsHandler;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import netweaver.server.Server;

@Slf4j
@RequiredArgsConstructor
public class PortUnificationHandler extends ByteToMessageDecoder {
	private final Server<?> server;
    private final boolean detectSsl, detectGzip;

    public PortUnificationHandler(Server<?> server) {
        this(server, true, true);
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        if(in.readableBytes() < 5)return;
        ChannelPipeline p = ctx.pipeline();
        if(detectSsl && SslHandler.isEncrypted(in)){
        	if(server.getSslContext() == null){
        		if(log.isWarnEnabled())log.warn(ctx.channel().remoteAddress().toString().substring(1) + " attempted to connect with SSL when SSL was not set.");	
        		in.clear();
                ctx.close();
        	}else{
        		p.addLast(server.getSslContext().newHandler(ctx.alloc()));
		        p.addLast(new PortUnificationHandler(server, false, detectGzip));
		        p.remove(this);
        	}
	        return;
        }else if(server.getSslContext() != null)p.addLast(server.getSslContext().newHandler(ctx.alloc()));
        
        final int c1 = in.getUnsignedByte(in.readerIndex()), c2 = in.getUnsignedByte(in.readerIndex() + 1);
        if(detectGzip && c1 == 31 && c2 == 139){
	        p.addLast(ZlibCodecFactory.newZlibEncoder(ZlibWrapper.GZIP));
	        p.addLast(ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP));
	        
	        p.addLast(new PortUnificationHandler(server, detectSsl, false));
        }else if(c1 == '{'){//TCP
	        p.addLast(new DelimiterBasedFrameDecoder(1024 * 256, Delimiters.lineDelimiter()));//TODO max buffer from constructor
	        
	        p.addLast(TcpCodec.get());
	        p.addLast(new ServerMessageHandler(server));
        }else if(//HTTP
        		c1 == 'G' && c2 == 'E' ||	// GET
	            c1 == 'P' && c2 == 'O' || 	// POST
	            c1 == 'P' && c2 == 'U' || 	// PUT
	            c1 == 'H' && c2 == 'E' || 	// HEAD
	            c1 == 'O' && c2 == 'P' || 	// OPTIONS
	            c1 == 'P' && c2 == 'A' || 	// PATCH
	            c1 == 'D' && c2 == 'E' || 	// DELETE
	            c1 == 'T' && c2 == 'R' || 	// TRACE
	            c1 == 'C' && c2 == 'O'){	// CONNECT
	        p.addLast(new HttpServerCodec());
			p.addLast(new HttpObjectAggregator(1024 * 256));//TODO max buffer from constructor
			p.addLast(new CorsHandler(CorsConfigBuilder.forAnyOrigin().build()));//TODO CORS policy from constructor
			//if(detectGzip)p.addLast(new HttpContentCompressor());TODO make GZIP work
			p.addLast(new ChunkedWriteHandler());
			
			p.addLast(new ServerHttpCodec(server, "ws"));//TODO webSocketPath from contstructor
			p.addLast(new ServerMessageHandler(server));
        }else{
        	if(log.isWarnEnabled())log.warn(ctx.channel().remoteAddress().toString().substring(1) + " attempted to connect with an unknown protocol.");	
            in.clear();
            ctx.close();
        }
        p.remove(this);
    }
}
