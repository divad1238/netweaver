package netweaver.server;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import netweaver.TimedService;

@Slf4j
public class SessionManager<SessionType extends Session> extends TimedService implements Iterable<SessionType>{
	@Getter
	Class<SessionType> sessionClass;
	
	private ConcurrentHashMap<String, SessionType> sessions;
	
	@Getter
	private volatile boolean isRunning = false;
	
	public boolean remove(String uid) {
		if(uid == null || uid.length() < 1)return false;
		return sessions.remove(uid) != null;
	}
	
	private final SessionType next() throws IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		try {
			SessionType s;
			do{
				s = getSessionClass().getDeclaredConstructor().newInstance();
				s.init(UUID.randomUUID().toString(), this);
			}while(sessions.putIfAbsent(s.getUid(), s) != null);
			return s;
		} catch (InstantiationException | IllegalAccessException e) {
			if(log.isErrorEnabled())log.error("Could not instantiate session of type " + getSessionClass().getName() + ". Please ensure that this class has a default constructor (no parameters).", e);
			throw new Error(e);
		}
	}

	public final SessionType get(String uid){
		if(uid == null || uid.length() < 1){
			try {
				return next();
			} catch (IllegalArgumentException | InvocationTargetException | NoSuchMethodException
					| SecurityException e) {
				throw new RuntimeException(e);
			}
		}else{
			SessionType session = null;
			if((session = sessions.get(uid)) != null)return session;
			else return onCacheMiss(uid);
		}
	}
	
	protected SessionType onCacheMiss(String uid){
		try {
			return next();
		} catch (IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void doStart(){
		if(isRunning)throw new IllegalAccessError("session manager already started.");
		sessions = new ConcurrentHashMap<>(2048);
		isRunning = true;
	}
	
	@Override
	protected void doStop(){
		sessions.clear();
		sessions = null;
		isRunning = false;
	}
	
	@Override
	public Iterator<SessionType> iterator() {
		return sessions.values().iterator();
	}

	@Override
	public void run() {
		if(log.isDebugEnabled())log.debug("Purging stale sessions...");
		
		long now = System.currentTimeMillis(), timeout = getInterval();
		
		for(SessionType session : sessions.values()){
			if(session != null && !session.isOpen() && now - session.getLastAccessTime() >= timeout && purge(session)){
				sessions.remove(session.getUid());
				session.destroy();
			}
		}
		
		if(log.isDebugEnabled())log.debug("Finished purging stale sessions. Runtime: " + (System.currentTimeMillis() - now) + "ms");
	}
	
	protected boolean purge(SessionType toPurge){
		return true;
	}

	@Override
	public long getInterval() {
		return 30 * 60;
	}
}
