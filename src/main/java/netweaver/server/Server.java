package netweaver.server;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.ssl.SslContext;

import java.util.concurrent.TimeUnit;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import netweaver.interfaces.Service;
import netweaver.server.netty.PortUnificationHandler;

@Slf4j
public class Server<SessionType extends Session> implements Service{//TODO re-cache master mustache when partials are modified
	@Getter
	private final int port;
	@Getter
	private final String defaultUri;
	@Getter
	private final SslContext sslContext;
	
	
	private EventLoopGroup bossGroup;
	private EventLoopGroup workerGroup;
	private Channel serverChannel;
	
	@Getter
	SessionManager<SessionType> sessionManager;

	@Getter
	private volatile boolean isStarted = false;
	
	public Server(int port){
		this(port, null, null);
	}
	
	public Server(int port, String defaultUri){
		this(port, defaultUri, null);
	}
	
	public Server(int port, SslContext sslCtx){
		this(port, null, sslCtx);
	}
	
	public Server(int port, String defaultUri, SslContext sslCtx){
		this.port = port;
		if(defaultUri == null)defaultUri = "";
		defaultUri = defaultUri.replace("\\", "/");
		this.defaultUri = defaultUri.length() > 0 && defaultUri.charAt(0) == '/' ? defaultUri.substring(1) : defaultUri;
		this.sslContext = sslCtx;
	}

	public void start(){
		if(isStarted)throw new IllegalAccessError("server already started.");
		
		bossGroup = new NioEventLoopGroup(1);
		workerGroup = new NioEventLoopGroup();
		ServerBootstrap b = new ServerBootstrap();
		b.group(bossGroup, workerGroup)
		.channel(NioServerSocketChannel.class)
		.childHandler(new ChannelInitializer<SocketChannel>(){

			@Override
			public void initChannel(SocketChannel ch) throws Exception {
				ch.pipeline().addLast(new PortUnificationHandler(Server.this, true, true));//TODO load checks for ssl && gzip from constructor
			}
		});
		serverChannel = b.bind(port).channel();

		isStarted = true;
		if(log.isInfoEnabled())log.info(getClass().getSimpleName() + " started on port " + port + "!");
	}

	@Override
	public void stop(){
		if(serverChannel != null){
			serverChannel.close();
			serverChannel = null;
		}
		if(workerGroup != null){
			workerGroup.shutdownGracefully();
			workerGroup = null;
		}
		if(bossGroup != null){
			bossGroup.shutdownGracefully();
			bossGroup = null;
		}
		isStarted = false;
		if(log.isInfoEnabled())log.info(getClass().getSimpleName() + " stopped on port  " + port + ".");
	}

	public void awaitClose() throws InterruptedException {
		awaitClose(0, null);
	}
	
	public void awaitClose(long timeout) throws InterruptedException {
		awaitClose(timeout, TimeUnit.MILLISECONDS);
	}
	
	public void awaitClose(long timeout, TimeUnit unit) throws InterruptedException {
		if(isStarted && serverChannel != null){
			if(timeout > 0)serverChannel.closeFuture().await(timeout, unit);
			else serverChannel.closeFuture().await();
		}
	}
	
	public final Class<SessionType> getSessionClass(){
		return sessionManager != null ? sessionManager.getSessionClass() : null;
	}
}