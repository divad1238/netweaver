package netweaver.server.controller;

import lombok.NonNull;
import netweaver.error.UserException;
import netweaver.metrics.RequestMapperMetric;
import netweaver.server.Netweaver;
import netweaver.server.Session;
import netweaver.transport.Header;
import netweaver.util.Util;

public abstract class Controller<RequestType, SessionType extends Session> extends BaseController<SessionType>{
	private Class<RequestType> requestClass = null;
	
	@SuppressWarnings("unchecked")
	@Override
	public Object handleRequest(@NonNull Header header, @NonNull SessionType session) throws UserException{
		Netweaver.validate(header, session.getPreferredLocale());
		Object body = getRequestClass() == String.class ? header.getBody() : Netweaver.fromJson(header.getBody(), requestClass);
		Netweaver.validate(body, session.getPreferredLocale());
		
		if(Netweaver.isMetered){
			RequestMapperMetric.Promise promise = RequestMapperMetric.start(header.getUri(), getClass().getSimpleName() + ".handleRequest()");
			try{
				return handleRequest((RequestType) body, session);
			}catch(Throwable t){
				promise.setError(true);
				throw t;
			}finally{
				RequestMapperMetric.finish(promise);
			}
		}else return handleRequest((RequestType) body, session);
	}

	@SuppressWarnings("unchecked")
	public final Class<RequestType> getRequestClass(){
		return requestClass == null ? requestClass = (Class<RequestType>) Util.getGenericClass(this, Controller.class, 0) : requestClass;
	}

	public abstract Object handleRequest(RequestType msg, SessionType session) throws UserException;
}
