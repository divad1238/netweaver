package netweaver.server.controller;

import netweaver.client.PartialCallback;
import netweaver.error.UserException;
import netweaver.server.Session;
import netweaver.transport.Header;
/**
 * Parses message body (unlike ({@link PartialProxyController}).
 * @author David
 *
 * @param <RequestType>
 * @param <ResponseType>
 * @param <SessionType>
 */
public abstract class ProxyController<RequestType, SessionType extends Session> extends Controller<RequestType, SessionType>{
	
	@Override
	public final Object handleRequest(RequestType msg, final SessionType session) throws UserException{
		final Session responder = getTargetSession(msg, session);
		if(responder != null){
			responder.request(getUri(), msg, new PartialCallback() {

				@Override
				public void handleResponse(Header header) throws Throwable {
					session.write(header.getUri(), header.getBody(), header.isError());
				}
				
			});
		}
		return null;
	}
	
	public abstract Session getTargetSession(RequestType msg, SessionType session) throws UserException;

}
