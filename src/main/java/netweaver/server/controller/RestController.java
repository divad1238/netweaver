package netweaver.server.controller;


import lombok.NonNull;
import netweaver.error.BadRequestException;
import netweaver.error.UserException;
import netweaver.metrics.RequestMapperMetric;
import netweaver.server.Netweaver;
import netweaver.server.Session;
import netweaver.transport.Header;
import netweaver.util.Util;

public abstract class RestController<GetType, PostType, PutType, DeleteType, SessionType extends Session> extends BaseController<SessionType> {
	private Class<GetType> getRequestClass = null;
	private Class<PostType> postRequestClass = null;
	private Class<PutType> putRequestClass = null;
	private Class<DeleteType> deleteRequestClass = null;
	
	@SuppressWarnings("unchecked")
	@Override
	public Object handleRequest(@NonNull Header header, @NonNull SessionType session) throws UserException{
		Netweaver.validate(header, session.getPreferredLocale());
		
		if(Netweaver.isMetered){
			RequestMapperMetric.Promise promise = RequestMapperMetric.start(header.getUri(), getClass().getSimpleName() + ".handleRequest()");
			try{
				Object msg;
				switch(header.getType()){
				case DELETE:
					if(getDeleteRequestClass() == String.class){
						msg = header.getBody();
					}else{
						msg = Netweaver.fromJson(header.getBody(), deleteRequestClass);
						Netweaver.validate(msg);
					}
					return doDelete((DeleteType) msg, session);
				case GET:
					if(getGetRequestClass() == String.class){
						msg = header.getBody();
					}else{
						msg = Netweaver.fromJson(header.getBody(), getRequestClass);
						Netweaver.validate(msg);
					}
					return doGet((GetType) msg, session);
				case POST:
					if(getPostRequestClass() == String.class){
						msg = header.getBody();
					}else{
						msg = Netweaver.fromJson(header.getBody(), postRequestClass);
						Netweaver.validate(msg);
					}
					return doPost((PostType) msg, session);
				case PUT:
					if(getPutRequestClass() == String.class){
						msg = header.getBody();
					}else{
						msg = Netweaver.fromJson(header.getBody(), putRequestClass);
						Netweaver.validate(msg);
					}
					return doPut((PutType) msg, session);
				default:
					throw new BadRequestException();
				}
			}catch(Throwable t){
				promise.setError(true);
				throw t;
			}finally{
				RequestMapperMetric.finish(promise);
			}
		}else{
			Object msg;
			switch(header.getType()){
			case DELETE:
				if(getDeleteRequestClass() == String.class){
					msg = header.getBody();
				}else{
					msg = Netweaver.fromJson(header.getBody(), deleteRequestClass);
					Netweaver.validate(msg);
				}
				return doDelete((DeleteType) msg, session);
			case GET:
				if(getGetRequestClass() == String.class){
					msg = header.getBody();
				}else{
					msg = Netweaver.fromJson(header.getBody(), getRequestClass);
					Netweaver.validate(msg);
				}
				return doGet((GetType) msg, session);
			case POST:
				if(getPostRequestClass() == String.class){
					msg = header.getBody();
				}else{
					msg = Netweaver.fromJson(header.getBody(), postRequestClass);
					Netweaver.validate(msg);
				}
				return doPost((PostType) msg, session);
			case PUT:
				if(getPutRequestClass() == String.class){
					msg = header.getBody();
				}else{
					msg = Netweaver.fromJson(header.getBody(), putRequestClass);
					Netweaver.validate(msg);
				}
				return doPut((PutType) msg, session);
			default:
				return handleRequest(header, session);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public Class<GetType> getGetRequestClass(){
		return getRequestClass == null ? getRequestClass = (Class<GetType>) Util.getGenericClass(this, RestController.class, 0) : getRequestClass;
	}

	@SuppressWarnings("unchecked")
	public Class<PostType> getPostRequestClass(){
		return postRequestClass == null ? postRequestClass = (Class<PostType>) Util.getGenericClass(this, RestController.class, 1) : postRequestClass;
	}
	
	@SuppressWarnings("unchecked")
	public Class<PutType> getPutRequestClass(){
		return putRequestClass == null ? putRequestClass = (Class<PutType>) Util.getGenericClass(this, RestController.class, 2) : putRequestClass;
	}
	
	@SuppressWarnings("unchecked")
	public Class<DeleteType> getDeleteRequestClass(){
		return deleteRequestClass == null ? deleteRequestClass = (Class<DeleteType>) Util.getGenericClass(this, RestController.class, 3) : deleteRequestClass;
	}
	
	public abstract Object doGet(GetType msg, SessionType session) throws UserException;
	public abstract Object doPost(PostType msg, SessionType session) throws UserException;
	public abstract Object doPut(PutType msg, SessionType session) throws UserException;
	public abstract Object doDelete(DeleteType msg, SessionType session) throws UserException;
}
