package netweaver.server.controller;

import netweaver.client.PartialCallback;
import netweaver.error.UserException;
import netweaver.metrics.RequestMapperMetric;
import netweaver.server.Netweaver;
import netweaver.server.Session;
import netweaver.transport.Header;

/**
 * Does not parse message body (unlike ({@link ProxyController}).
 * @author David
 *
 * @param <RequestType>
 * @param <ResponseType>
 * @param <SessionType>
 */
public abstract class PartialProxyController<SessionType extends Session> extends BaseController<SessionType>{
	
	@Override
	public final Object handleRequest(Header header, final SessionType session) throws UserException{
		Netweaver.validate(header, session.getPreferredLocale());
		
		if(Netweaver.isMetered){
			RequestMapperMetric.Promise promise = RequestMapperMetric.start(header.getUri(), getClass().getSimpleName() + ".handleRequest()");
			try{
				final SessionType responder = getTargetSession(header.getBody(), session);
				if(responder != null){
					responder.request(header.getUri(), header.getBody(), header.isError(), new PartialCallback() {

						@Override
						public void handleResponse(Header header) throws Throwable {
							session.write(header.getUri(), header.getBody(), header.isError());
						}
						
					});
				}
			}catch(Throwable t){
				promise.setError(true);
				throw t;
			}finally{
				RequestMapperMetric.finish(promise);
			}
		}else {
			final SessionType responder = getTargetSession(header.getBody(), session);
			if(responder != null){
				responder.request(header.getUri(), header.getBody(), header.isError(), new PartialCallback() {

					@Override
					public void handleResponse(Header header) throws Throwable {
						session.write(header.getUri(), header.getBody(), header.isError());
					}
					
				});
			}
		}
		return null;
	}
	
	public abstract SessionType getTargetSession(String body, SessionType session) throws UserException;

}
