package netweaver.server.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.regex.Pattern;

import com.google.common.base.CaseFormat;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import netweaver.error.UserException;
import netweaver.server.Session;
import netweaver.template.Template;
import netweaver.transport.Header;
import netweaver.util.Util;

public abstract class BaseController<SessionType extends Session> {
private static final Pattern urlSanitizer = Pattern.compile("[//]+");
	
	@Getter @Setter
	private String contentType = "application/json";
	@Getter @Setter
	private Template template = null;
	
	@Getter 
	private String uri;
	
	private Class<SessionType> sessionClass = null;
	
	@Getter
	private boolean isClone = false;
	
	{
		uri = getClass().getPackage().getName();
		
		if(uri.contains("www")){
			uri = uri.substring(uri.indexOf("www") + 3);
			if(uri.length() > 0 && uri.charAt(0) == '.'){
				uri = uri.substring(1);
			}
		}
		
		if(!uri.toUpperCase().endsWith(getClass().getSimpleName().replace(Controller.class.getSimpleName(), "").toUpperCase())){
			uri += (uri.length() > 0 ? "." : "") + getClass().getSimpleName().replace(Controller.class.getSimpleName(), "");
		}	

		uri = CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, uri).replace("._", "/").replace('.', '/');
		uri = urlSanitizer.matcher(uri).replaceAll("/");
	}
	
	@SuppressWarnings("unchecked")
	public final Class<SessionType> getSessionClass(){
		return sessionClass == null ? sessionClass = (Class<SessionType>) Util.getGenericClass(this, BaseController.class, 0) : sessionClass;
	}

	public abstract Object handleRequest(@NonNull Header header, @NonNull SessionType session) throws UserException;
	
	public final BaseController<SessionType> clone(String uri) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException{
		@SuppressWarnings("unchecked")
		BaseController<SessionType> instance = getClass().getDeclaredConstructor().newInstance();
		instance.uri = uri;
		instance.isClone = true;
		return instance;
	}
	
	public final boolean isTemplate(){
		return template != null;
	}

}
