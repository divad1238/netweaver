package netweaver.template.mustache;

import java.io.IOException;
import java.io.Writer;

import com.github.mustachejava.MustacheException;

public class PlainTextMustacheFactory extends HtmlMustacheFactory{
	@Override
	public void encode(String value, Writer writer) {
		try {
			writer.append(value, 0, value.length());
		} catch (IOException e) {
			 throw new MustacheException("Failed to encode value: " + value);
		}
	}
}