package netweaver.template.mustache;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import netweaver.server.Netweaver;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheException;

public class HtmlMustacheFactory extends DefaultMustacheFactory{

	@Override
	public Mustache compile(Reader reader, String name) {
		return compile(reader, name, Netweaver.templateStart, Netweaver.templateEnd);
	}
	
	@Override
	public Mustache compile(String file){
		try(FileReader fr = new FileReader(file)){
			return this.compile(fr, file, Netweaver.templateStart, Netweaver.templateEnd);
		} catch (IOException e) {
			Throwable cause = e.getCause();
			if (cause instanceof MustacheException) {
				throw (MustacheException) cause;
			}
			throw new MustacheException(cause);
		}
	}

	@Override
	public String resolvePartialPath(String dir, String name, String extension) {
		String filePath = name;

		if (name.startsWith("/"))filePath = Netweaver.resDir + name;
		else if(dir.isEmpty())filePath = Netweaver.resDir + "/" + name;
		else filePath = dir + name;

		return new File(filePath).getAbsolutePath().replace("\\", "/");
	}
}
