package netweaver.template;

import java.util.Locale;

import netweaver.server.Session;
import netweaver.transport.Header;

public interface Template {
	public String evaluate(Header h, Session s, Object r, Locale l) throws Exception;
	public TemplateType getTemplateType();
}
