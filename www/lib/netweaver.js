var Logger = (function () {
	return {
		log : function (msg) {
			var now = new Date();
			console.log('[' + (now.getHours() < 10 ? ('0' + now.getHours()) : now.getHours()) + ':' +
				(now.getMinutes() < 10 ? ('0' + now.getMinutes()) : now.getMinutes()) + ':' +
				(now.getSeconds() < 10 ? ('0' + now.getSeconds()) : now.getSeconds()) + ']: ' + msg);
		},
		
		consoleDump: function(obj){
			var out = '';
			for (var i in obj) {
				out += i + ": " + obj[i] + "\n";
			}
			console.log(out);
		},
		
		alertDump: function(obj){
			var out = '';
			for (var i in obj) {
				out += i + ": " + obj[i] + "\n";
			}
			alert(out);
		}
	};
})();

var Netweaver = (function (lgr) {
	if (!window.WebSocket) {
		window.WebSocket = window.MozWebSocket;
	}

	var Logger = lgr,
		HOST = window.location.host,
		canWebSocket = window.WebSocket ? true : false,
		canCookie = navigator.cookieEnabled,
		webSocket = null,
		isSSL = false,
		callbacks = [],
		uid = "",
		sessionDestroyedHandler = null;

	function ClientException(key, message) {
		this.key = key;
		this.message = message;
	}

	function getAjaxObject() {
		return window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	}

	function newWebSocket(onOpenCallback, onCloseCallback) {
		if (!canWebSocket)
			throw new ClientException('WebSocketException', 'WebSocket unsupported');
		if (webSocket)
			webSocket.close();

		var ws = new WebSocket('ws' + (isSSL ? 's' : '') + '://' + HOST + '/ws');
		ws.onmessage = function (event) {
			Logger.log('received <- ' + event.data);
			var index = event.data.indexOf('}{') + 1,
				header = JSON.parse(event.data.substr(0, index)),
				body = JSON.parse(event.data.substr(index)),
				callback = callbacks[header.uri];
			
			if(canCookie)setCookie("uid", header.uid, 365);
			
			if(uid != header.uid){
				var oldUid = uid;
				uid = header.uid;
				if(oldUid.length > 0 && typeof(sessionDestroyedHandler) == 'function')sessionDestroyedHandler();
			}
				
			if(callback){
				if (callback.timeout){
					callbacks[header.uri] = null;
					clearTimeout(callback.timeout);
				}	
			
				if(!header.error){
					if (typeof(callback.success) == 'function'){
						callback.success(body);
					}
				}else{
					if(typeof(callback.error) == 'function'){
						callback.error(body);
					}
				}
			}
		};
		
		ws.onopen = function (event) {
			webSocket = ws;
			if (onOpenCallback && typeof(onOpenCallback == 'function'))
				onOpenCallback();
		};
		
		ws.onclose = function (event) {
			webSocket = null;
			if (onCloseCallback && typeof(onCloseCallback == 'function'))
				onCloseCallback();
		};
	}

	function getCookie(cname) {
		var name = cname + '=';
		var ca = document.cookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ')
				c = c.substring(1);
			if (c.indexOf(name) == 0)
				return c.substring(name.length, c.length);
		}
		return '';
	}

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = 'expires=' + d.toUTCString();
		document.cookie = cname + '=' + cvalue + '; ' + expires + '; path=/';
	}
	
	return {
		log: function(msg){
			Logger.log(msg);
		},
		
		canWebSocket : window.WebSocket ? true : false,
		
		setHandler: function(uri, onSuccess, onError, isSingleUse){
			var temp = callbacks[uri];
			if(temp && temp.timeout)clearTimeout(temp.timeout);
			var callback = callbacks[uri] = {
				success: onSuccess,
				error: onError
			};
			if(isSingleUse)callback.timeout = setTimeout(function(){}, 0);
		},

		get: function(uri, message, onSuccess, onError, timeout){
			var xmlhttp = getAjaxObject();
			
			xmlhttp.onreadystatechange = (function () {
				if (xmlhttp.readyState != 4)
					return; //4 = finished, 3 = processing, 2 received, 1 = connected, 0 = not initialized
				Logger.log('received <- ' + xmlhttp.responseText);
				
				var body = JSON.parse(xmlhttp.responseText);
				if(xmlhttp.status < 300){
					if (typeof(onSuccess) == 'function'){
						onSuccess(body);
					}
				}else{
					if(typeof(onError) == 'function'){
						onError(body);
					}
				}
			}).bind(this);

			xmlhttp.open('GET', 'http' + (isSSL ? 's' : '') + '://' + HOST + '/' + uri + '?' + Object.keys(message).map(function(key){return encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]);}).join('&'), true);
			xmlhttp.setRequestHeader("content-type", "application/json; charset=UTF-8");
			xmlhttp.send();

			Logger.log('sent -> ' + JSON.stringify(message));
		},

		post: function(uri, message, onSuccess, onError, timeout){
			var xmlhttp = getAjaxObject();
			
			xmlhttp.onreadystatechange = (function () {
				if (xmlhttp.readyState != 4)
					return; //4 = finished, 3 = processing, 2 received, 1 = connected, 0 = not initialized
				Logger.log('received <- ' + xmlhttp.responseText);
				
				var body = JSON.parse(xmlhttp.responseText);
				if(xmlhttp.status < 300){
					if (typeof(onSuccess) == 'function'){
						onSuccess(body);
					}
				}else{
					if(typeof(onError) == 'function'){
						onError(body);
					}
				}
			}).bind(this);

			xmlhttp.open('POST', 'http' + (isSSL ? 's' : '') + '://' + HOST + '/' + uri, true);
			xmlhttp.setRequestHeader("content-type", "application/json; charset=UTF-8");
			xmlhttp.send(JSON.stringify(message));

			Logger.log('sent -> ' + JSON.stringify(message));
		},

		put: function(uri, message, onSuccess, onError, timeout){
			var xmlhttp = getAjaxObject();
			
			xmlhttp.onreadystatechange = (function () {
				if (xmlhttp.readyState != 4)
					return; //4 = finished, 3 = processing, 2 received, 1 = connected, 0 = not initialized
				Logger.log('received <- ' + xmlhttp.responseText);
				
				var body = JSON.parse(xmlhttp.responseText);
				if(xmlhttp.status < 300){
					if (typeof(onSuccess) == 'function'){
						onSuccess(body);
					}
				}else{
					if(typeof(onError) == 'function'){
						onError(body);
					}
				}
			}).bind(this);

			xmlhttp.open('PUT', 'http' + (isSSL ? 's' : '') + '://' + HOST + '/' + uri, true);
			xmlhttp.setRequestHeader("content-type", "application/json; charset=UTF-8");
			xmlhttp.send(JSON.stringify(message));

			Logger.log('sent -> ' + JSON.stringify(message));
		},

		delete: function(uri, message, onSuccess, onError, timeout){
			var xmlhttp = getAjaxObject();
			
			xmlhttp.onreadystatechange = (function () {
				if (xmlhttp.readyState != 4)
					return; //4 = finished, 3 = processing, 2 received, 1 = connected, 0 = not initialized
				Logger.log('received <- ' + xmlhttp.responseText);
				
				var body = JSON.parse(xmlhttp.responseText);
				if(xmlhttp.status < 300){
					if (typeof(onSuccess) == 'function'){
						onSuccess(body);
					}
				}else{
					if(typeof(onError) == 'function'){
						onError(body);
					}
				}
			}).bind(this);

			xmlhttp.open('DELETE', 'http' + (isSSL ? 's' : '') + '://' + HOST + '/' + uri, true);
			xmlhttp.setRequestHeader("content-type", "application/json; charset=UTF-8");
			xmlhttp.send(JSON.stringify(message));

			Logger.log('sent -> ' + JSON.stringify(message));
		},

		request : function (uri, message, onSuccess, onError, timeout) {
			if(callbacks[uri])return;
			if(!message)message = {};

			

			if (canWebSocket) {
				var fn = (function () {
					webSocket.send(JSON.stringify({uri: uri, uid: canCookie ? uid = getCookie("uid") : uid}) + JSON.stringify(message));
				}).bind(this);
				if (webSocket && webSocket.readyState == WebSocket.OPEN) {
					fn();
				} else {
					newWebSocket(fn);
				}
			} else {
				var xmlhttp = getAjaxObject();
			
				xmlhttp.onreadystatechange = (function () {
					if (xmlhttp.readyState != 4)
						return; //4 = finished, 3 = processing, 2 received, 1 = connected, 0 = not initialized
					Logger.log('received <- ' + xmlhttp.responseText);
					
					var index = event.data.indexOf('}{') + 1,
						header = JSON.parse(event.data.substr(0, index)),
						body = JSON.parse(event.data.substr(index)),
						callback = callbacks[header.uri];
					
					if(uid != header.uid){
						var oldUid = uid;
						uid = header.uid;
						if(oldUid.length > 0 && typeof(sessionDestroyedHandler) == 'function')sessionDestroyedHandler();
					}
						
					if(callback){
						if (callback.timeout){
							clearTimeout(callback.timeout);
							callbacks[header.uri] = null;
						}	
					
						if(!header.error){
							if (typeof(callback.success) == 'function'){
								callback.success(body);
							}
						}else{
							if(typeof(callback.error) == 'function'){
								callback.error(body);
							}
						}
					}
				}).bind(this);

				xmlhttp.open('POST', 'http' + (isSSL ? 's' : '') + '://' + HOST + '/' + uri, true);
				xmlhttp.setRequestHeader("content-type", "application/json; charset=UTF-8");
				xmlhttp.send(JSON.stringify(message));

				Logger.log('sent -> ' + JSON.stringify(message));
			}

			callbacks[uri] = {
				success: onSuccess, 
				error: onError,
				timeout: setTimeout((function () {
					var callback = callbacks[uri];
					if(!callback || !callback.timeout)return;
					callbacks[uri] = null;
					
					if (typeof(callback.error) == 'function')
						callback.error({
							key : 'NetworkException',
							message : 'Network error. Please try again'
						});
					}
				).bind(this), timeout ? timeout : 5000)
			};
		},
		
		setSessionDestroyedHandler: function(handler){
			sessionDestroyedHandler = handler;
		}
	};
})(Logger);
